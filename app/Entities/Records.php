<?php namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Records extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'adult_form';
    protected static $table_name = 'adult_form';
    protected $primaryKey = 'entry_id';

    protected $fillable = [];

}
