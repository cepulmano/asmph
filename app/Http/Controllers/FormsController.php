<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Redirect;

use App\Entities\Records;

class FormsController extends Controller {

    public function welcome()
    {
        return view('welcome');
    }


    public function loadform($id)
    {
        $data['intent'] = 'new';
        return view('form', ['formid' => $id])->with($data);
    }

    public function updateAdult($entry_id)
    {
        $values = Records::find($entry_id);
        $data['entry_id'] = $entry_id;
        $data['decoded'] = json_decode($values->form_values);
        $data['intent'] = 'update';

        return view('form', ['formid' => '01'])->with($data);
    }

    public function saveAdult()
    {
        $timeStamp = time();
        $entryID = $timeStamp;

        $form = $_POST;
        $time = $form['time'];
        $time = json_decode($form['time'], true);

        $intent = $form['intent'];
        $entry_id_existing = $form['entry_id'];

        //remove time from $form
        unset($form['time']);
        unset($form['intent']);
        unset($form['entry_id']);

        $form_string = json_encode($form);
        $time_string = json_encode($time);

        if($intent == 'new')
        {
            $new_record = new Records();
            $new_record->entry_id = $entryID;
            $new_record->form_values = $form_string;
            $new_record->form_time = $time_string;
        }
        else
        {
            $new_record = Records::find($entry_id_existing);
            $new_record->form_values = $form_string;       
        }

        // dd($new_record);
        $new_record->save();

        return Redirect::to('admin');

    }

    public function showResult()
    {
        $data['records'] = Records::lists('form_values','entry_id');

        return view('results')->with($data);
    }

    public function delete($entry_id)
    {
        $record = Records::find($entry_id);
        $record->delete();

        return Redirect::to('admin');  
    }
}
