<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'FormsController@welcome');
Route::get('/loadform/{id}', 'FormsController@loadform');
Route::post('/save', 'FormsController@saveAdult');
Route::get('/update/{id}', 'FormsController@updateAdult');
Route::get('/delete/{id}', 'FormsController@delete');

Route::get('/admin', 'FormsController@showResult');
