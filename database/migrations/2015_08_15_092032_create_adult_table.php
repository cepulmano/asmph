<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdultTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adult_form', function (Blueprint $table) {
            //GENERAL INFORMATION
            $table->increments('id');
            $table->string('entry_id', 30);
            $table->string('name', 20);
            $table->string('date_of_birth', 20);
            $table->string('student_in_charge', 20);
            $table->string('date_of_admission', 20);
            $table->string('age', 20);
            $table->string('sex', 20);
            $table->string('civil_status', 20);
            $table->string('religion', 20);
            $table->string('address', 60);
            $table->string('date_of_interview',20);
            $table->string('place_of_interview', 60);
            $table->string('informant', 20);
            $table->string('relation', 30);
            $table->string('attending_physician', 30);
            $table->enum('reliability', ['Excellent', 'Very Good', 'Good', 'Fair', 'Poor']);

            /*
            //CLINICAL HISTORY
            $table->text('chief_complaint');
            $table->text('illness_history');
            $table->text('temporal_profile');
            $table->text('on_legend');

            //REVIEW OF SYSTEMS --- General
            $table->tinyInteger('fever');
            $table->tinyInteger('weight_gain');
            $table->tinyInteger('weight_loss');
            $table->tinyInteger('weakness');
            $table->tinyInteger('fatigue');
            $table->tinyInteger('gen_others');
            $table->text('gen_others_text');

            //REVIEW OF SYSTEMS --- Muscoloskeletal/integumentary
            $table->tinyInteger('ashes');
            $table->tinyInteger('lumps');
            $table->tinyInteger('sores');
            $table->tinyInteger('itching');
            $table->tinyInteger('muscle_pains');
            $table->tinyInteger('jointpains');
            $table->tinyInteger('changes_in_color');
            $table->tinyInteger('joint_swelling');
            $table->tinyInteger('changes_in_hair_nails');
            $table->tinyInteger('mus_others');
            $table->text('mus_others_text');

            //REVIEW OF SYSTEMS --- HEENT
            $table->tinyInteger('headache');
            $table->tinyInteger('dizziness');
            $table->tinyInteger('blurring_of_vision');
            $table->tinyInteger('tinnitus');
            $table->tinyInteger('deafness');
            $table->tinyInteger('epistaxis');
            $table->tinyInteger('frequent_colds');
            $table->tinyInteger('hoarseness');
            $table->tinyInteger('dry_mouth');
            $table->tinyInteger('gum_bleeding');
            $table->tinyInteger('enlarge_LN');
            $table->tinyInteger('heent_others');
            $table->text('heent_others_text');

            //REVIEW OF SYSTMES --- Respiratory
            $table->tinyInteger('dyspnea');
            $table->tinyInteger('hemoptysis');
            $table->tinyInteger('cough');
            $table->tinyInteger('wheezing');
            $table->tinyInteger('respi_others');
            $table->text('respi_others_text');

            //REVIEW OF SYSTEMS --- Cardiovascular
            $table->tinyInteger('palpitations');
            $table->tinyInteger('chest_pains');
            $table->tinyInteger('syncope');
            $table->tinyInteger('orthopnea');
            $table->tinyInteger('cardio_others');
            $table->text('cardio_others_text');

            //REVIEW OF SYSTEMS --- Gastrointestinal
            $table->tinyInteger('nausea');
            $table->tinyInteger('vomiting');
            $table->tinyInteger('dysphagia');
            $table->tinyInteger('heartburn');
            $table->tinyInteger('constipation');
            $table->tinyInteger('diarrhea');
            $table->tinyInteger('rectal_bleeding');
            $table->tinyInteger('jaundice');
            $table->tinyInteger('gastro_others');
            $table->text('gastro_others_text');

            //REVIEW OF SYSTEMS --- Endocrine
            $table->tinyInteger('excessive_sweating');
            $table->tinyInteger('heat_intolerance');
            $table->tinyInteger('plyuria');
            $table->tinyInteger('excessive_thirst');
            $table->tinyInteger('cold_intolerance');
            $table->tinyInteger('endoc_others');
            $table->text('endoc_others_text');

            //REVIEW OF SYSTEMS --- Genitourinary
            $table->tinyInteger('dysurua');
            $table->tinyInteger('sexual_dysfunction');
            $table->tinyInteger('discharge');
            $table->tinyInteger('geni_others');
            $table->tinyInteger('geni_others_text');

            //REVIEW OF SYSTEMS --- Neurological
            $table->tinyInteger('seizures');
            $table->tinyInteger('tremors');
            $table->tinyInteger('neuro_others');
            $table->tinyInteger('neuro_others_text');

            //PAST MEDICAL HISTORY
            $table->tinyInteger('thyroid_disease');
            $table->tinyInteger('tuberculosis');
            $table->tinyInteger('asthma');
            $table->tinyInteger('pneumonia');
            $table->tinyInteger('gastroenteritis');
            $table->tinyInteger('diabetes');
            $table->tinyInteger('hypertension');
            $table->tinyInteger('stroke');
            $table->tinyInteger('psychiatric_condition');
            $table->tinyInteger('kidney_disease');
            $table->tinyInteger('liver_disease');
            $table->tinyInteger('allergies');
            $table->text('allergies_text');
            $table->tinyInteger('cancer_site');
            $table->text('cancer_site_text');
            $table->tinyInteger('musculo_skeletal_disease');
            $table->tinyInteger('pmh_others');
            $table->text('pmh_others_text');

            //PAST MEDICAL HISTORY --- Prior Hospitalization
            $table->string('ph_date1', 20);
            $table->text('ph_text1');
            $table->string('ph_date2', 20);
            $table->text('ph_text2');

            //PAST MEDICAL HISTORY --- Prior Surgery
            $table->string('ps_date1', 20);
            $table->text('ps_text1');
            $table->string('ps_date2', 20);
            $table->text('ps_text2');

            //PAST MEDICAL HISTORY
            $table->text('review_of_current_medications');
            $table->text('present_labs');

            //FAMILY HISTORY
            $table->text('fh_genogram');
            $table->text('fh_legend');

            //IMMUNIZATION HISTORY
            $table->text('influenza_dose');
            $table->text('influenza_year');
            $table->text('pneumonia_dose');
            $table->text('pneumonia_year');
            $table->text('ih_others1');
            $table->text('ih_others1_dose');
            $table->text('ih_others1_year');
            $table->tinyInteger('vaccinated_by_privatemd');
            $table->text('hepatitisB_dose');
            $table->text('hepatitisB_year');
            $table->text('mmr_dose');
            $table->text('mmr_year');
            $table->text('ih_others2');
            $table->text('ih_others2_dose');
            $table->text('ih_others2_year');
            $table->tinyInteger('vaccinated_by_health_center');

            //PERSONAL HISTORY
            $table->text('education');
            $table->text('smoking');
            $table->text('alcohol_intake');
            $table->text('substance_use');
            $table->text('physical_activity_exercise');
            $table->text('diet');

            //SOCIAL AND ENVIRONMENT HISTORY
            $table->text('occupation');
            $table->text('number_of_household_members');
            $table->text('electricity');
            $table->text('exposure_to_substance');
            $table->text('type_of_dwelling');
            $table->text('drinking_water_source');
            $table->text('waste_garbage_disposal');
            $table->text('pertinent_history_of_travel');
            $table->text('seh_others');

            //REPRODUCTIVE HISTORY
            $table->text('lmp');
            $table->text('first_coitus_age');
            $table->text('pmp');
            $table->text('number_sexual_partners');
            $table->text('menarche');
            $table->text('interval');
            $table->text('duration_amount');
            $table->text('contraceptive');
            $table->text('sexual_diseases_disorders');
            $table->text('rh_others');

            //OBSTETRIC HISTORY --- G1
            $table->text('g1_date');
            $table->text('g1_type_delivery');
            $table->text('g1_aog');
            $table->text('g1_place_delivery');
            $table->text('g1_sex');
            $table->text('g1_birthweight');
            $table->text('g1_remarks');

            //OBSTETRIC HISTORY --- G2
            $table->text('g2_date');
            $table->text('g2_type_delivery');
            $table->text('g2_aog');
            $table->text('g2_place_delivery');
            $table->text('g2_sex');
            $table->text('g2_birthweight');
            $table->text('g2_remarks');

            //OBSTETRIC HISTORY --- G3
            $table->text('g3_date');
            $table->text('g3_type_delivery');
            $table->text('g3_aog');
            $table->text('g3_place_delivery');
            $table->text('g3_sex');
            $table->text('g3_birthweight');
            $table->text('g3_remarks');

            //OBSTETRIC HISTORY --- G4
            $table->text('g4_date');
            $table->text('g4_type_delivery');
            $table->text('g4_aog');
            $table->text('g4_place_delivery');
            $table->text('g4_sex');
            $table->text('g4_birthweight');
            $table->text('g4_remarks');

            //STAKEHOLDER ANALYSIS --- SH_1
            $table->text('sh_1_name_role');
            $table->text('sh_1_1stake');
            $table->text('sh_1_stand_issue');
            $table->text('sh_1_intensity_stand');
            $table->text('sh_1_degree_influence');
            $table->text('sh_1_insight_action');


            //STAKEHOLDER ANALYSIS --- SH_2
            $table->text('sh_2_name_role');
            $table->text('sh_2_stake');
            $table->text('sh_2_stand_issue');
            $table->text('sh_2_intensity_stand');
            $table->text('sh_2_degree_influence');
            $table->text('sh_2_insight_action');


            //STAKEHOLDER ANALYSIS --- SH_3
            $table->text('sh_3_name_role');
            $table->text('sh_3_stake');
            $table->text('sh_3_stand_issue');
            $table->text('sh_3_intensity_stand');
            $table->text('sh_3_degree_influence');
            $table->text('sh_3_insight_action');


            //STAKEHOLDER ANALYSIS --- SH_4
            $table->text('sh_4_name_role');
            $table->text('sh_4_stake');
            $table->text('sh_4_stand_issue');
            $table->text('sh_4_intensity_stand');
            $table->text('sh_4_degree_influence');
            $table->text('sh_4_insight_action');

            //GENERAL SURVEY --- Anthropometrics
            $table->text('height');
            $table->text('weight');
            $table->text('bmi');

            $table->text('hc');
            $table->text('cc');
            $table->text('ac');

            //GENERAL SURVEY --- Vital Signs
            $table->text('hr');
            $table->text('rr');
            $table->text('t');
            $table->text('bp');

            $table->tinyInteger('pain_scale');

            $table->text('head_and_neck');
            $table->text('eyes');
            $table->text('cardiovascular');
            $table->text('breasts_axillae');
            $table->text('chest_lungs');
            $table->text('back_spine');
            $table->text('abdomen');
            $table->text('pelvis_gu_tract');
            $table->text('rectal');
            $table->text('upper_extremities');
            $table->text('integumentary');
            $table->text('integ_skin');
            $table->text('integ_nails');
            $table->text('integ_hair_scalp');
            */

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
