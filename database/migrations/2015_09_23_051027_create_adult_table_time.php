<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdultTableTime extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adult_form_time', function (Blueprint $table) {
            //GENERAL INFORMATION
            $table->increments('id', 30);
            $table->string('entry_id', 30);
            $table->string('name', 30);
            $table->string('date_of_birth', 30);
            $table->string('student_in_charge', 30);
            $table->string('date_of_admission', 30);
            $table->string('age', 30);
            $table->string('sex', 30);
            $table->string('civil_status', 30);
            $table->string('religion', 30);
            $table->string('address', 60);
            $table->string('date_of_interview',30);
            $table->string('place_of_interview', 60);
            $table->string('informant', 30);
            $table->string('relation', 30);
            $table->string('reliability', 30);
            $table->string('attending_physician', 30);

            /*
            //CLINICAL HISTORY
            $table->string('chief_complaint', 30);
            $table->string('illness_history', 30);
            $table->string('temporal_profile', 30);
            $table->string('on_legend', 30);

            //REVIEW OF SYSTEMS --- General
            $table->string('fever', 30);
            $table->string('weight_gain', 30);
            $table->string('weight_loss', 30);
            $table->string('weakness', 30);
            $table->string('fatigue', 30);
            $table->string('gen_others', 30);
            $table->string('gen_others_text', 30);

            //REVIEW OF SYSTEMS --- Muscoloskeletal/integumentary
            $table->string('ashes', 30);
            $table->string('lumps', 30);
            $table->string('sores', 30);
            $table->string('itching', 30);
            $table->string('muscle_pains', 30);
            $table->string('jointpains', 30);
            $table->string('changes_in_color', 30);
            $table->string('joint_swelling', 30);
            $table->string('changes_in_hair_nails', 30);
            $table->string('mus_others', 30);
            $table->string('mus_others_text', 30);

            //REVIEW OF SYSTEMS --- HEENT
            $table->string('headache', 30);
            $table->string('dizziness', 30);
            $table->string('blurring_of_vision', 30);
            $table->string('tinnitus', 30);
            $table->string('deafness', 30);
            $table->string('epistaxis', 30);
            $table->string('frequent_colds', 30);
            $table->string('hoarseness', 30);
            $table->string('dry_mouth', 30);
            $table->string('gum_bleeding', 30);
            $table->string('enlarge_LN', 30);
            $table->string('heent_others', 30);
            $table->string('heent_others_text', 30);

            //REVIEW OF SYSTMES --- Respiratory
            $table->string('dyspnea', 30);
            $table->string('hemoptysis', 30);
            $table->string('cough', 30);
            $table->string('wheezing', 30);
            $table->string('respi_others', 30);
            $table->string('respi_others_text', 30);

            //REVIEW OF SYSTEMS --- Cardiovascular
            $table->string('palpitations', 30);
            $table->string('chest_pains', 30);
            $table->string('syncope', 30);
            $table->string('orthopnea', 30);
            $table->string('cardio_others', 30);
            $table->string('cardio_others_text', 30);

            //REVIEW OF SYSTEMS --- Gastrointestinal
            $table->string('nausea', 30);
            $table->string('vomiting', 30);
            $table->string('dysphagia', 30);
            $table->string('heartburn', 30);
            $table->string('constipation', 30);
            $table->string('diarrhea', 30);
            $table->string('rectal_bleeding', 30);
            $table->string('jaundice', 30);
            $table->string('gastro_others', 30);
            $table->string('gastro_others_text', 30);

            //REVIEW OF SYSTEMS --- Endocrine
            $table->string('excessive_sweating', 30);
            $table->string('heat_intolerance', 30);
            $table->string('plyuria', 30);
            $table->string('excessive_thirst', 30);
            $table->string('cold_intolerance', 30);
            $table->string('endoc_others', 30);
            $table->string('endoc_others_text', 30);

            //REVIEW OF SYSTEMS --- Genitourinary
            $table->string('dysurua', 30);
            $table->string('sexual_dysfunction', 30);
            $table->string('discharge', 30);
            $table->string('geni_others', 30);
            $table->string('geni_others_text', 30);

            //REVIEW OF SYSTEMS --- Neurological
            $table->string('seizures', 30);
            $table->string('tremors', 30);
            $table->string('neuro_others', 30);
            $table->string('neuro_others_text', 30);

            //PAST MEDICAL HISTORY
            $table->string('thyroid_disease', 30);
            $table->string('tuberculosis', 30);
            $table->string('asthma', 30);
            $table->string('pneumonia', 30);
            $table->string('gastroenteritis', 30);
            $table->string('diabetes', 30);
            $table->string('hypertension', 30);
            $table->string('stroke', 30);
            $table->string('psychiatric_condition', 30);
            $table->string('kidney_disease', 30);
            $table->string('liver_disease', 30);
            $table->string('allergies', 30);
            $table->string('allergies_text', 30);
            $table->string('cancer_site', 30);
            $table->string('cancer_site_text', 30);
            $table->string('musculo_skeletal_disease', 30);
            $table->string('pmh_others', 30);
            $table->string('pmh_others_text', 30);

            //PAST MEDICAL HISTORY --- Prior Hospitalization
            $table->string('ph_date1', 30);
            $table->string('ph_text1', 30);
            $table->string('ph_date2', 30);
            $table->string('ph_text2', 30);

            //PAST MEDICAL HISTORY --- Prior Surgery
            $table->string('ps_date1', 30);
            $table->string('ps_text1', 30);
            $table->string('ps_date2', 30);
            $table->string('ps_text2', 30);

            //PAST MEDICAL HISTORY
            $table->string('review_of_current_medications', 30);
            $table->string('present_labs', 30);

            //FAMILY HISTORY
            $table->string('fh_genogram', 30);
            $table->string('fh_legend', 30);

            //IMMUNIZATION HISTORY
            $table->string('influenza_dose', 30);
            $table->string('influenza_year', 30);
            $table->string('pneumonia_dose', 30);
            $table->string('pneumonia_year', 30);
            $table->string('ih_others1', 30);
            $table->string('ih_others1_dose', 30);
            $table->string('ih_others1_year', 30);
            $table->string('vaccinated_by_privatemd', 30);
            $table->string('hepatitisB_dose', 30);
            $table->string('hepatitisB_year', 30);
            $table->string('mmr_dose', 30);
            $table->string('mmr_year', 30);
            $table->string('ih_others2', 30);
            $table->string('ih_others2_dose', 30);
            $table->string('ih_others2_year', 30);
            $table->string('vaccinated_by_health_center', 30);

            //PERSONAL HISTORY
            $table->string('education', 30);
            $table->string('smoking', 30);
            $table->string('alcohol_intake', 30);
            $table->string('substance_use', 30);
            $table->string('physical_activity_exercise', 30);
            $table->string('diet', 30);

            //SOCIAL AND ENVIRONMENT HISTORY
            $table->string('occupation', 30);
            $table->string('number_of_household_members', 30);
            $table->string('electricity', 30);
            $table->string('exposure_to_substance', 30);
            $table->string('type_of_dwelling', 30);
            $table->string('drinking_water_source', 30);
            $table->string('waste_garbage_disposal', 30);
            $table->string('pertinent_history_of_travel', 30);
            $table->string('seh_others', 30);

            //REPRODUCTIVE HISTORY
            $table->string('lmp', 30);
            $table->string('first_coitus_age', 30);
            $table->string('pmp', 30);
            $table->string('number_sexual_partners', 30);
            $table->string('menarche', 30);
            $table->string('interval', 30);
            $table->string('duration_amount', 30);
            $table->string('contraceptive', 30);
            $table->string('sexual_diseases_disorders', 30);
            $table->string('rh_others', 30);

            //OBSTETRIC HISTORY --- G1
            $table->string('g1_date', 30);
            $table->string('g1_type_delivery', 30);
            $table->string('g1_aog', 30);
            $table->string('g1_place_delivery', 30);
            $table->string('g1_sex', 30);
            $table->string('g1_birthweight', 30);
            $table->string('g1_remarks', 30);

            //OBSTETRIC HISTORY --- G2
            $table->string('g2_date', 30);
            $table->string('g2_type_delivery', 30);
            $table->string('g2_aog', 30);
            $table->string('g2_place_delivery', 30);
            $table->string('g2_sex', 30);
            $table->string('g2_birthweight', 30);
            $table->string('g2_remarks', 30);

            //OBSTETRIC HISTORY --- G3
            $table->string('g3_date', 30);
            $table->string('g3_type_delivery', 30);
            $table->string('g3_aog', 30);
            $table->string('g3_place_delivery', 30);
            $table->string('g3_sex', 30);
            $table->string('g3_birthweight', 30);
            $table->string('g3_remarks', 30);

            //OBSTETRIC HISTORY --- G4
            $table->string('g4_date', 30);
            $table->string('g4_type_delivery', 30);
            $table->string('g4_aog', 30);
            $table->string('g4_place_delivery', 30);
            $table->string('g4_sex', 30);
            $table->string('g4_birthweight', 30);
            $table->string('g4_remarks', 30);

            //STAKEHOLDER ANALYSIS --- SH_1
            $table->string('sh_1_name_role', 30);
            $table->string('sh_1_1stake', 30);
            $table->string('sh_1_stand_issue', 30);
            $table->string('sh_1_intensity_stand', 30);
            $table->string('sh_1_degree_influence', 30);
            $table->string('sh_1_insight_action', 30);


            //STAKEHOLDER ANALYSIS --- SH_2
            $table->string('sh_2_name_role', 30);
            $table->string('sh_2_stake', 30);
            $table->string('sh_2_stand_issue', 30);
            $table->string('sh_2_intensity_stand', 30);
            $table->string('sh_2_degree_influence', 30);
            $table->string('sh_2_insight_action', 30);


            //STAKEHOLDER ANALYSIS --- SH_3
            $table->string('sh_3_name_role', 30);
            $table->string('sh_3_stake', 30);
            $table->string('sh_3_stand_issue', 30);
            $table->string('sh_3_intensity_stand', 30);
            $table->string('sh_3_degree_influence', 30);
            $table->string('sh_3_insight_action', 30);


            //STAKEHOLDER ANALYSIS --- SH_4
            $table->string('sh_4_name_role', 30);
            $table->string('sh_4_stake', 30);
            $table->string('sh_4_stand_issue', 30);
            $table->string('sh_4_intensity_stand', 30);
            $table->string('sh_4_degree_influence', 30);
            $table->string('sh_4_insight_action', 30);

            //GENERAL SURVEY --- Anthropometrics
            $table->string('height', 30);
            $table->string('weight', 30);
            $table->string('bmi', 30);

            $table->string('hc', 30);
            $table->string('cc', 30);
            $table->string('ac', 30);

            //GENERAL SURVEY --- Vital Signs
            $table->string('hr', 30);
            $table->string('rr', 30);
            $table->string('t', 30);
            $table->string('bp', 30);

            $table->string('pain_scale', 30);

            $table->string('head_and_neck', 30);
            $table->string('eyes', 30);
            $table->string('cardiovascular', 30);
            $table->string('breasts_axillae', 30);
            $table->string('chest_lungs', 30);
            $table->string('back_spine', 30);
            $table->string('abdomen', 30);
            $table->string('pelvis_gu_tract', 30);
            $table->string('rectal', 30);
            $table->string('upper_extremities', 30);
            $table->string('integumentary', 30);
            $table->string('integ_skin', 30);
            $table->string('integ_nails', 30);
            $table->string('integ_hair_scalp', 30);
            */

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
