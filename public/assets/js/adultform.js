var previousField = null;
var previousFieldTimeStart = 0;
var focusedField = null;
var focusedFieldTimeStart = 0;
var timeArray = new Array();
var formTimeStart = 0;

$(function() {
	formTimeStart = $.now();
	
	// Update pain scale
	checkpainscale();

	$('#formSubmit').on('click', function() {

		var timeCheck = $('#time').value;

		var jObject={};
		for(i in timeArray)
		{
			jObject[i] = timeArray[i];
		}
		timeCheck = JSON.stringify(jObject);

		$('#time').val(timeCheck);
		$(this).closest('form').submit();
	});

	$(':input').on('focus', function() {
		var a = $(this).attr('id');
		computeTime(a);
	});

    $(".painimg").hover(
        function() { $(this).addClass("Hover"); },
        function() { $(this).removeClass("Hover"); 
    });


	$(".painimg").click(function() {
		var pain_value = $(this).attr('value');
		$('#pe_pain_scale_value').val(pain_value);

		var a = $(this).attr('id');
		computeTime(a);
		$(".painimg").css('border', 'none');
		$(this).css('border', "solid 2px black");  
	});

	$('#date_of_birth').on('change', function() {
		var birthdate = $(this).attr('value');

	    var today = new Date();
	    var birthDate = new Date(birthdate);
	    var age = today.getFullYear() - birthDate.getFullYear();
	    var m = today.getMonth() - birthDate.getMonth();
	    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
	        age--;
	    }
	    $('#age').val(age);
	});

	$('#vacc_unknown').on('click', function() {
		if ($(this).attr("checked")) {
		    $(this).closest(".whiteback").find(':radio').attr("disabled", false);
		} else {
			$(this).closest(".whiteback").find(':radio').attr("checked", false);
		    $(this).closest(".whiteback").find(':radio').attr("disabled", true)
		}
	});

	$('#pe_height').on('change', function() {
		var height = $('#pe_height').attr('value');
		var weight = $('#pe_weight').attr('value');
		var bmi = weight/((height/100)*(height/100));

	    $('#pe_bmi').val(bmi.toFixed(2));
	});

	$('#pe_weight').on('change', function() {
		var height = $('#pe_height').attr('value');
		var weight = $('#pe_weight').attr('value');
		var bmi = weight/((height/100)*(height/100));

	    $('#pe_bmi').val(bmi.toFixed(2));
	});

	$('#generalAll').click(function() {
		if ($('#generalAll').attr("checked")) {
		    $(this).closest(".whiteback").find(':checkbox').attr("checked", false);
		    $(this).attr("checked", true);
		    $(this).closest(".whiteback").find(':checkbox').attr("disabled", true);
		    $(this).attr("disabled", false);
		} else {
		    $(this).closest(".whiteback").find(':checkbox').attr("disabled", false)
		}
	});

	$('#msk_intAll').click(function() {
		if ($('#msk_intAll').attr("checked")) {
		    $(this).closest(".whiteback").find(':checkbox').attr("checked", false);
		    $(this).attr("checked", true);
		    $(this).closest(".whiteback").find(':checkbox').attr("disabled", true);
		    $(this).attr("disabled", false);
		} else {
		    $(this).closest(".whiteback").find(':checkbox').attr("disabled", false)
		}
	});

	$('#heentAll').click(function() {
		if ($('#heentAll').attr("checked")) {
		    $(this).closest(".whiteback").find(':checkbox').attr("checked", false);
		    $(this).attr("checked", true);
		    $(this).closest(".whiteback").find(':checkbox').attr("disabled", true);
		    $(this).attr("disabled", false);
		} else {
		    $(this).closest(".whiteback").find(':checkbox').attr("disabled", false)
		}
	});

	$('#respiratoryAll').click(function() {
		if ($('#respiratoryAll').attr("checked")) {
		    $(this).closest(".whiteback").find(':checkbox').attr("checked", false);
		    $(this).attr("checked", true);
		    $(this).closest(".whiteback").find(':checkbox').attr("disabled", true);
		    $(this).attr("disabled", false);
		} else {
		    $(this).closest(".whiteback").find(':checkbox').attr("disabled", false)
		}
	});

	$('#cardiovascularAll').click(function() {
		if ($('#cardiovascularAll').attr("checked")) {
		    $(this).closest(".whiteback").find(':checkbox').attr("checked", false);
		    $(this).attr("checked", true);
		    $(this).closest(".whiteback").find(':checkbox').attr("disabled", true);
		    $(this).attr("disabled", false);
		} else {
		    $(this).closest(".whiteback").find(':checkbox').attr("disabled", false)
		}
	});

	$('#gastrointestinalAll').click(function() {
		if ($('#gastrointestinalAll').attr("checked")) {
		    $(this).closest(".whiteback").find(':checkbox').attr("checked", false);
		    $(this).attr("checked", true);
		    $(this).closest(".whiteback").find(':checkbox').attr("disabled", true);
		    $(this).attr("disabled", false);
		} else {
		    $(this).closest(".whiteback").find(':checkbox').attr("disabled", false)
		}
	});

	$('#endocrineAll').click(function() {
		if ($(this).attr("checked")) {
		    $(this).closest(".whiteback").find(':checkbox').attr("checked", false);
		    $(this).attr("checked", true);
		    $(this).closest(".whiteback").find(':checkbox').attr("disabled", true);
		    $(this).attr("disabled", false);
		} else {
		    $(this).closest(".whiteback").find(':checkbox').attr("disabled", false)
		}
	});

	$('#genitourinaryAll').click(function() {
		if ($(this).attr("checked")) {
		    $(this).closest(".whiteback").find(':checkbox').attr("checked", false);
		    $(this).attr("checked", true);
		    $(this).closest(".whiteback").find(':checkbox').attr("disabled", true);
		    $(this).attr("disabled", false);
		} else {
		    $(this).closest(".whiteback").find(':checkbox').attr("disabled", false)
		}
	});

	$('#neurologicalAll').click(function() {
		if ($(this).attr("checked")) {
		    $(this).closest(".whiteback").find(':checkbox').attr("checked", false);
		    $(this).attr("checked", true);
		    $(this).closest(".whiteback").find(':checkbox').attr("disabled", true);
		    $(this).attr("disabled", false);
		} else {
		    $(this).closest(".whiteback").find(':checkbox').attr("disabled", false)
		}
	});

})

function computeTime(id)
{
	//SET PREVIOUS FOCUSED FIELD
	if(focusedField != null)
	{
		previousField = focusedField;
	}

	previousFieldTimeStart = focusedFieldTimeStart;

	//SET NEW FOCUSED FIELD
	focusedField = id;
	focusedFieldTimeStart = $.now();

	var previousTime = focusedFieldTimeStart - previousFieldTimeStart;

	//COMPUTE AND RECORD TIME FOR PREVIOUS FOCUSED FIELD
	if(previousField != null)
	{
		if(timeArray[previousField] != null)
		{
			var cumulative = timeArray[previousField] + previousTime;
			timeArray[previousField] = cumulative;
		}
		else
		{
			timeArray[previousField] = previousTime;
		}
	}
	console.log(timeArray);
}


function checkpainscale()
{
	pain_scale_value = $('#pe_pain_scale_value').attr('value');;
	if(pain_scale_value == '0')
	{
		$('.painimg0').css('border', "solid 2px black"); 		
	}
	else if(pain_scale_value == '1')
	{
		$('.painimg1').css('border', "solid 2px black"); 
	}
	else if(pain_scale_value == '2')
	{
		$('.painimg2').css('border', "solid 2px black"); 
	}
	else if(pain_scale_value == '3')
	{
		$('.painimg3').css('border', "solid 2px black"); 
	}
	else if(pain_scale_value == '4')
	{
		$('.painimg4').css('border', "solid 2px black"); 
	}
	else if(pain_scale_value == '5')
	{
		$('.painimg5').css('border', "solid 2px black"); 		
	}
	else
	{
		$(".painimg").css('border', 'none');
	}
}