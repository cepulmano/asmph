<?php
    $date = date('Y-m-d H:i:s');

    // if(isset($decoded))
    // {
    //     dd($decoded);
    // }
?>

<!DOCTYPE html>
<html>
    <head>
        <title>ASMPH : Adult Clinical Encounter Form</title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
        <link href="../../resources/assets/plugins/datepicker3.css" rel="stylesheet">
        <link href="../../resources/assets/plugins/bootstrap-datetimepicker.css" rel="stylesheet">
        <style>
            html, body {
                height: 100%;
            }
            body {
                color:#111;
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 500;
                font-family: 'Lato';
            }
            .container {
                text-align: center;
                margin-bottom:25px;
            }
            .content {

            }
            .title {
                font-size: 42px;
            }
            .subtitle {
                font-size: 20px;
                font-weight:500;
                margin-bottom: 25px;
            }
            .show-grid div[class^=grid] {
                padding: 0px;
                background-color: #eee;
                border: 1px solid #ddd;
            }
            div.header {
                color: #fff;
                font-weight: 700;
                padding:10px;
                background-color: blue;
                text-transform: uppercase;
            }
            div.header2 {
                color: #000;
                font-weight: 700;
                padding:10px;
                text-transform: uppercase;
            }

            textarea{
                height:127px !important;
                margin:0;   
            }

            div.largetextarea{
                height:129px !important;  
            }

            div.double-height {
                height: 82px !important;
            }
            div.triple-height {
                height: 128px !important;
            }
            label.double-height {
                height: 82px !important;
            }
            label.triple-height {
                height: 128px !important;
            }
            .whiteback {
                background-color: #fff !important;
            }
            .blueback {
                background-color: #99BBDF !important;
            }
            input[type=text], textarea, label, select {
                border: none;
                height: 40px;
                padding: 10px;
            }
            input[type=text].col-md-4 {
                width: 33.33333333%;
            }
            div > .row input[type=text].col-md-6 {
                border: 0px solid #000000;
                border-bottom-width: 0.1px;
            }
            label, input {
                height: auto;
            }

            label {
                margin-bottom: 0 !important;
                float:left;
                text-align: right;
            }
            label.blocked {
                float:none;
                text-align: left;
            }
            .nogrid label {
                min-height: 44.5px;
            }
            .nowidth { width: auto !important; }
            .noborder { border: 0 !important; }
            .textleft {
                text-align: left;
            }
            .checkbox {
                padding:0 10px !important;
                margin-bottom:0 !important;
                height: 42px;
                margin-top: 0px !important;
            }
            input.inlinefield {
                width: 70%;
                border-bottom: 1px solid #BBB;
                margin-top: -7px;
                padding:0 10px 4px !important;
                height:24px;
            }
            .datetimepicker {
                border-radius: 0 !important;
                direction: ltr;
                margin-top: 0 !important;
            }
            span.rightlabel {
                width: 50px;
                display: inline-block;
            }
            .tabler {
                border-collapse: separate;
                border-spacing: 0;
                display: table;
                margin: 0 -15px;
            }
            .trow {
                display: table-row;
            }
            .trow > div {
                display: table-cell;
                background-color: #fff !important;
                padding: 30px;
                height: 700px;
            }
            .trow > div > label {
                background-color: #eee;
                width: 100%;
            }
            .trow > div > div > div > input[type=text] {
                width: 100%;
                border: 0px solid #000000;
                border-bottom-width: 1px;
            }
            input::-moz-placeholder, textarea::-moz-placeholder {
                opacity: 0.3 !important;
                font-style: italic;
            }
            img.Hover { 
                border: 1px solid #000; 
            }
        </style>
    </head>
    <body>
        <div class="container">
            {!! Form::open(array( 'method' => 'POST', 'url'=>'save', 'id'=>'ASMPHForm', 'name'=>'ASMPHForm', 'class'=>'form-horizontal' )) !!}
            <input type="hidden" name="time" id="time" value="">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="intent" value="<?php if(isset($intent)) {echo $intent;} ?>">
            <input type="hidden" name="entry_id" value="<?php if(isset($entry_id)) {echo $entry_id;} ?>">
            <div class="row show-grid">
                <div class="col-md-6">
                    <div class="title"><a href="{{ url('/') }}"><img src={{ asset("assets/images/asmph.jpg") }} width="150" /></a></div>
                    <div class="subtitle">Adult Clinical Encounter Form</div>
                </div>
                <div class="grid col-md-6">
                    <label class="col-md-4 textleft">Interview Date and Time</label><input type="text" placeholder="Date of Interview" class="datetimepicker col-md-8 form-input" name="interview_date" id="interview_date" value="<?php if(isset($decoded->interview_date)) { echo $decoded->interview_date; } else { echo $date; } ?>" readonly />                
                    <label class="col-md-4 textleft">Student/Interviewer</label><input type="text" placeholder="Student-in-charge" class="col-md-8 form-input" name="student_in_charge" id="student_in_charge" value="<?php if(isset($decoded->student_in_charge)) { echo $decoded->student_in_charge; } ?>" />
                    <!-- <label class="col-md-4">Admission Date</label><input type="text" placeholder="Date of Admission" class="datepicker col-md-8 form-input" name="admission_date" id="admission_date" /> -->
                    <label class="col-md-4 textleft">Place of Interview</label><input type="text" placeholder="Place of Interview" class="col-md-8 form-input" name="place_of_interview" id="place_of_interview" value="<?php if(isset($decoded->place_of_interview)) { echo $decoded->place_of_interview; } ?>" />
                </div>
            </div>

            <div class="form-group">
                <div class="row show-grid">
                    <div class="header col-md-12">General Information</div>
                </div>
                <div class="row show-grid">
                    <div class="grid col-md-12">
                        <label class="col-md-3 textleft" >Name</label>
                        <input type="text" placeholder="First" class="col-md-3 form-input" name="first_name" id="first_name" value="<?php if(isset($decoded->first_name)) { echo $decoded->first_name; } ?>" />
                        <input type="text" placeholder="Middle" class="col-md-3 form-input" name="middle_name" id="middle_name" value="<?php if(isset($decoded->middle_name)) { echo $decoded->middle_name; } ?>" />
                        <input type="text" placeholder="Last" class="col-md-3 form-input" name="last_name" id="last_name" value="<?php if(isset($decoded->last_name)) { echo $decoded->last_name; } ?>" />
                    </div>
                </div>
                <div class="row show-grid">
                    <div class="grid col-md-12">
                    <label class="col-md-3 textleft" >Birth Date</label><input type="text" placeholder="Date of Birth (MM/DD/YYYY)" class="datepicker col-md-9 form-input" name="date_of_birth" id="date_of_birth" value="<?php if(isset($decoded->date_of_birth)) { echo $decoded->date_of_birth; } ?>" />
                    </div>
                </div>
                <div class="row show-grid">
                    <div class="grid col-md-12">
                        <label class="col-md-1 textleft">Age</label>
                        <input type="text" placeholder="Age" class="col-md-2 form-input" name="age" id="age" value="<?php if(isset($decoded->age)) { echo $decoded->age; } ?>" readonly />
                        <label class="col-md-1 textleft">Sex</label>
      
                        <select class="col-md-3 form-input" name="sex" id="sex">
                            <option value="">Select Sex</option>
                            <option value="Male" <?php if(isset($decoded->sex) && $decoded->sex == 'Male') {echo "selected";} ?> >Male</option>
                            <option value="Female" <?php if(isset($decoded->sex) && $decoded->sex == 'Female') {echo "selected";} ?> >Female</option>
                        </select>
                        <label class="col-md-2 textleft">Civil Status</label>
                        <select class="col-md-3 form-input" name="civil_status" id="civil_status">
                            <option value="">Select Status</option>
                            <option value="Single" <?php if(isset($decoded->civil_status) && $decoded->civil_status == 'Single') {echo "selected";} ?> >Single</option>
                            <option value="Married" <?php if(isset($decoded->civil_status) && $decoded->civil_status == 'Married') {echo "selected";} ?> >Married</option>
                            <option value="Divorced" <?php if(isset($decoded->civil_status) && $decoded->civil_status == 'Divorced') {echo "selected";} ?> >Divorced</option>
                            <option value="Separated" <?php if(isset($decoded->civil_status) && $decoded->civil_status == 'Separated') {echo "selected";} ?> >Separated</option>
                            <option value="Widowed" <?php if(isset($decoded->civil_status) && $decoded->civil_status == 'Widowed') {echo "selected";} ?> >Widowed</option>
                            <option value="Cohabiting" <?php if(isset($decoded->civil_status) && $decoded->civil_status == 'Cohabiting') {echo "selected";} ?> >Cohabiting</option>
                        </select>
                    </div>
                </div>
                <div class="row show-grid">
                    <div class="grid col-md-12">
                        <label class="col-md-1 textleft">Religion</label>
                        <select class="col-md-2 form-input" name="religion" id="religion">
                            <option value="">Select Status</option>
                            <option value="Catholic" <?php if(isset($decoded->religion) && $decoded->religion == 'Catholic') {echo "selected";} ?> >Catholic</option>
                            <option value="Christian" <?php if(isset($decoded->religion) && $decoded->religion == 'Christian') {echo "selected";} ?> >Christian</option>
                            <option value="Muslim" <?php if(isset($decoded->religion) && $decoded->religion == 'Muslim') {echo "selected";} ?> >Muslim</option>
                            <option value="Protestant" <?php if(isset($decoded->religion) && $decoded->religion == 'Protestant') {echo "selected";} ?> >Protestant</option>
                            <option value="Other" <?php if(isset($decoded->religion) && $decoded->religion == 'Other') {echo "selected";} ?> >Other</option>
                        </select>
                        <input type="text" placeholder="Other Specify" class="col-md-2 form-input" name="other_religion" id="other_religion" value="<?php if(isset($decoded->other_religion)) { echo $decoded->other_religion; } ?>" />
                        <label class="col-md-2 textleft">Occupation</label>
                        <input type="text" placeholder="Occupation" class="col-md-5 form-input" name="occupation" id="occupation" value="<?php if(isset($decoded->occupation)) { echo $decoded->occupation; } ?>" />
                    </div>
                </div>
                <div class="row show-grid">
                    <div class="grid col-md-6 triple-height">
                        <label class="col-md-3 textleft">Address</label>
                        <textarea type="text" placeholder="Address" class="col-md-9 form-input" name="address" id="address" ><?php if(isset($decoded->address)) {echo $decoded->address;} ?> </textarea>
                    </div>
                    <div class="grid col-md-6">
                        <label class="col-md-3 textleft">Informant</label>
                        <input type="text" placeholder="Name of informant, patient's name if he/she is informant" class="col-md-9 form-input" name="informant" id="informant" value="<?php if(isset($decoded->informant)) { echo $decoded->informant; } ?>" />
                        <label class="col-md-3 textleft">Relation</label>
                        <input type="text" placeholder="Relation to patient" class="col-md-9 form-input" name="relation_to_patient" id="relation_to_patient" value="<?php if(isset($decoded->relation_to_patient)) { echo $decoded->relation_to_patient; } ?>" />
                        <label class="col-md-3 textleft">Reliability</label>
                        <div class="col-md-9 whiteback noborder">
                            <div class="radio-inline">
                                <label>
                                  <input type="radio" name="reliability" id="reliability" value="Poor" <?php if(isset($decoded->reliability) && $decoded->reliability == 'Poor') {echo "checked";} ?> > Poor
                                </label>
                            </div>
                            <div class="radio-inline">
                                <label>
                                  <input type="radio" name="reliability" id="reliability" value="Fair" <?php if(isset($decoded->reliability) && $decoded->reliability == 'Fair') {echo "checked";} ?> > Fair
                                </label>
                            </div>
                            <div class="radio-inline">
                                <label>
                                  <input type="radio" name="reliability" id="reliability" value="Very Good" <?php if(isset($decoded->reliability) && $decoded->reliability == 'Very Good') {echo "checked";} ?> > Very Good
                                </label>
                            </div>
                            <div class="radio-inline">
                                <label>
                                  <input type="radio" name="reliability" id="reliability" value="Excellent" <?php if(isset($decoded->reliability) && $decoded->reliability == 'Excellent') {echo "checked";} ?> > Excellent
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row show-grid">                   
                    <div class="grid col-md-12">
                        <label class="col-md-3 textleft">Attending Physicians</label>
                        <input type="text" placeholder="Name/s of attending physician/s" class="col-md-9 form-input" name="attending_physician" id="attending_physician" value="<?php if(isset($decoded->attending_physician)) { echo $decoded->attending_physician; } ?>" />
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row show-grid">
                    <div class="header col-md-12">Clinical History</div>
                </div>
                <div class="row show-grid">
                    <div class="grid col-md-12"><label class="col-md-2">Chief Complaint(s)</label><input type="text" placeholder="Patient's own words if possible" class="col-md-10 form-input" name="chief_complaint" id="chief_complaint" value="<?php if(isset($decoded->chief_complaint)) { echo $decoded->chief_complaint; } ?>" /></div>
                </div>
                <div class="row show-grid">
                    <div class="grid col-md-12 largetextarea"><label class="col-md-2">History of Present Illness</label><textarea type="text" placeholder="Chronological account of the patient's present illness" class="col-md-10 form-input" name="history_present_illness" id="history_present_illness"><?php if(isset($decoded->history_present_illness)) {echo $decoded->history_present_illness;} ?></textarea></div>
                </div>
            </div>

            <div class="form-group">
                <div class="row show-grid">
                    <div class="header grid col-md-12">Review of Systems</div>
                </div>

                <div class="show-grid tabler">
                    <div class="trow">
                        <div class="grid col-md-2">
                            <label class="blocked">General</label>
                            <div class="whiteback">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="general[]" id="generalAll" value="All Unremarkable"
                                        <?php
                                        if(isset($decoded->general))
                                        {
                                            foreach ($decoded->general as $key => $value) {
                                                if($value == "All Unremarkable")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                        ?>
                                        > All Unremarkable
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="general[]" id="general" value="Fatigue"
                                      <?php
                                        if(isset($decoded->general))
                                        {
                                            foreach ($decoded->general as $key => $value) {
                                                if($value == "Fatigue")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                      ?>
                                      > Fatigue
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="general[]" id="general" value="Fever"
                                      <?php
                                        if(isset($decoded->general))
                                        {
                                            foreach ($decoded->general as $key => $value) {
                                                if($value == "Fever")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                      ?>
                                      > Fever
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="general[]" id="general" value="Weakness"
                                      <?php
                                        if(isset($decoded->general))
                                        {
                                            foreach ($decoded->general as $key => $value) {
                                                if($value == "Weakness")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                      ?>
                                      > Weakness
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="general[]" id="general" value="Weight Gain"
                                      <?php
                                        if(isset($decoded->general))
                                        {
                                            foreach ($decoded->general as $key => $value) {
                                                if($value == "Weight Gain")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                      ?>
                                      > Weight Gain
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="general[]" id="general" value="Weight Loss"
                                      <?php
                                        if(isset($decoded->general))
                                        {
                                            foreach ($decoded->general as $key => $value) {
                                                if($value == "Weight Loss")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                      ?>
                                      > Weight Loss
                                    </label>
                                </div>
                                <label> Others </label>
                                <div class="col-md-12">
                                    <input type="textarea" name="general_others" id="general" value="<?php if(isset($decoded->general_others)) { echo $decoded->general_others; } ?>" >
                                </div>
                            </div>
                        </div>

                        <div class="grid col-md-2">
                            <label class="blocked">MSK/INT</label>
                            <div class="whiteback">
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="msk_int[]" id="msk_intAll" value="All Unremarkable"
                                        <?php
                                        if(isset($decoded->msk_int))
                                        {
                                            foreach ($decoded->msk_int as $key => $value) {
                                                if($value == "All Unremarkable")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }   
                                        ?>
                                      > All Unremarkable
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="msk_int[]" id="msk_int" value="Change in color"
                                        <?php
                                        if(isset($decoded->msk_int))
                                        {                                        
                                            foreach ($decoded->msk_int as $key => $value) {
                                                if($value == "Change in color")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                        ?>
                                      > Changes in color
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="msk_int[]" id="msk_int" value="Changes in Hair &amp; Nails"
                                        <?php
                                        if(isset($decoded->msk_int))
                                        {
                                            foreach ($decoded->msk_int as $key => $value) {
                                                if($value == "Changes in Hair & Nails")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                        ?>
                                      > Changes in Hair &amp; Nails
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="msk_int[]" id="msk_int" value="Itching"
                                        <?php
                                        if(isset($decoded->msk_int))
                                        {
                                            foreach ($decoded->msk_int as $key => $value) {
                                                if($value == "Itching")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                        ?>
                                      > Itching
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="msk_int[]" id="msk_int" value="Join Pains"
                                        <?php
                                        if(isset($decoded->msk_int))
                                        {
                                            foreach ($decoded->msk_int as $key => $value) {
                                                if($value == "Join Pains")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                        ?>
                                      > Join Pains
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="msk_int[]" id="msk_int" value="Joint Swelling"
                                        <?php
                                        if(isset($decoded->msk_int))
                                        {
                                            foreach ($decoded->msk_int as $key => $value) {
                                                if($value == "Joint Swelling")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                        ?>
                                      > Joint Swelling
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="msk_int[]" id="msk_int" value="Lumps"
                                        <?php
                                        if(isset($decoded->msk_int))
                                        {
                                            foreach ($decoded->msk_int as $key => $value) {
                                                if($value == "Lumps")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                        ?>
                                      > Lumps
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="msk_int[]" id="msk_int" value="Muscle Pains"
                                        <?php
                                        if(isset($decoded->msk_int))
                                        {
                                            foreach ($decoded->msk_int as $key => $value) {
                                                if($value == "Muscle Pains")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                        ?>
                                      > Muscle Pains
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="msk_int[]" id="msk_int" value="Sores"
                                        <?php
                                        if(isset($decoded->msk_int))
                                        {
                                            foreach ($decoded->msk_int as $key => $value) {
                                                if($value == "Sores")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                        ?>
                                      > Sores
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="msk_int[]" id="msk_int" value="Rashes"
                                        <?php
                                        if(isset($decoded->msk_int))
                                        {
                                            foreach ($decoded->msk_int as $key => $value) {
                                                if($value == "Rashes")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                        ?>
                                      > Rashes
                                    </label>
                                </div>                                                              
                                <label> Others </label>
                                <div class="col-md-12">
                                    <input type="textarea" name="msk_int_others" id="msk_int" value="<?php if(isset($decoded->msk_int_others)) { echo $decoded->msk_int_others; } ?>">
                                </div>
                            </div>
                        </div>

                        <div class="grid col-md-2">
                            <label class="blocked">HEENT</label>
                            <div class="whiteback">
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="heent[]" id="heentAll" value="All Unremarkable"
                                        <?php
                                        if(isset($decoded->heent))
                                        {
                                            foreach ($decoded->heent as $key => $value) {
                                                if($value == "All Unremarkable")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                        ?>
                                      > All Unremarkable
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="heent[]" id="heent" value="Blurring of vision"
                                        <?php
                                        if(isset($decoded->heent))
                                        {
                                            foreach ($decoded->heent as $key => $value) {
                                                if($value == "Blurring of vision")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                        ?>
                                      > Blurring of vision
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="heent[]" id="heent" value="Deafness"
                                        <?php
                                        if(isset($decoded->heent))
                                        {
                                            foreach ($decoded->heent as $key => $value) {
                                                if($value == "Deafness")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                        ?>
                                      > Deafness
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="heent[]" id="heent" value="Dizziness"
                                        <?php
                                        if(isset($decoded->heent))
                                        {
                                            foreach ($decoded->heent as $key => $value) {
                                                if($value == "Dizziness")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                        ?>
                                      > Dizziness
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="heent[]" id="heent" value="Dry Mouth"
                                        <?php
                                        if(isset($decoded->heent))
                                        {
                                            foreach ($decoded->heent as $key => $value) {
                                                if($value == "Dry Mouth")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                        ?>
                                      > Dry Mouth
                                    </label>
                                </div>                               
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="heent[]" id="heent" value="Enlarged LN"
                                        <?php
                                        if(isset($decoded->heent))
                                        {
                                            foreach ($decoded->heent as $key => $value) {
                                                if($value == "Enlarged LN")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                        ?>
                                      > Enlarged LN
                                    </label>
                                </div>                                
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="heent[]" id="heent" value="Epistaxis"
                                        <?php
                                        if(isset($decoded->heent))
                                        {
                                            foreach ($decoded->heent as $key => $value) {
                                                if($value == "Epistaxis")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                        ?>
                                      > Expistaxis
                                    </label>
                                </div> 
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="heent[]" id="heent" value="Frequent Colds"
                                        <?php
                                        if(isset($decoded->heent))
                                        {
                                            foreach ($decoded->heent as $key => $value) {
                                                if($value == "Frequent Colds")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                        ?>
                                      > Frequent Colds
                                    </label>
                                </div>   
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="heent[]" id="heent" value="Gum Bleeding"
                                        <?php
                                        if(isset($decoded->heent))
                                        {
                                            foreach ($decoded->heent as $key => $value) {
                                                if($value == "Gum Bleeding")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                        ?>
                                      > Gum Bleeding
                                    </label>
                                </div>                                                                                            
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="heent[]" id="heent" value="Headache"
                                        <?php
                                        if(isset($decoded->heent))
                                        {                                        
                                            foreach ($decoded->heent as $key => $value) {
                                                if($value == "Headache")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                        ?>
                                      > Headache
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="heent[]" id="heent" value="Hoarseness"
                                        <?php
                                        if(isset($decoded->heent))
                                        {                                        
                                            foreach ($decoded->heent as $key => $value) {
                                                if($value == "Hoarseness")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                        ?>
                                      > Hoarseness
                                    </label>
                                </div>                                
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="heent[]" id="heent" value="Tinnitus"
                                        <?php
                                        if(isset($decoded->heent))
                                        {
                                            foreach ($decoded->heent as $key => $value) {
                                                if($value == "Tinnitus")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                        ?>
                                      > Tinnitus
                                    </label>
                                </div>
                                <label> Others </label>
                                <div class="col-md-12">
                                    <input type="textarea" name="heent_others" id="heent" value="<?php if(isset($decoded->heent_others)) { echo $decoded->heent_others; } ?>" >
                                </div>
                            </div>
                        </div>

                        <div class="grid col-md-2">
                            <label class="blocked">Respiratory</label>
                            <div class="whiteback">
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="respiratory[]" id="respiratoryAll" value="All Unremarkable"
                                        <?php
                                        if(isset($decoded->respiratory))
                                        {
                                            foreach ($decoded->respiratory as $key => $value) {
                                                if($value == "All Unremarkable")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                        ?>
                                      > All Unremarkable
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="respiratory[]" id="respiratory" value="Cough"
                                        <?php
                                        if(isset($decoded->respiratory))
                                        {
                                            foreach ($decoded->respiratory as $key => $value) {
                                                if($value == "Cough")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                        ?>
                                      > Cough
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="respiratory[]" id="respiratory" value="Dyspnea"
                                        <?php
                                        if(isset($decoded->respiratory))
                                        {
                                            foreach ($decoded->respiratory as $key => $value) {
                                                if($value == "Dyspnea")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                        ?>
                                      > Dyspnea
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="respiratory[]" id="respiratory" value="Hemoptysis"
                                        <?php
                                        if(isset($decoded->respiratory))
                                        {
                                            foreach ($decoded->respiratory as $key => $value) {
                                                if($value == "Hemoptysis")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                        ?>
                                      > Hemoptysis
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="respiratory[]" id="respiratory" value="Wheezing"
                                        <?php
                                        if(isset($decoded->respiratory))
                                        {
                                            foreach ($decoded->respiratory as $key => $value) {
                                                if($value == "Wheezing")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                        ?>
                                      > Weezhing
                                    </label>
                                </div>
                                <label> Others </label>
                                <div class="col-md-12">
                                    <input type="textarea" name="respiratory_others" id="respiratory" value="<?php if(isset($decoded->respiratory_others)) { echo $decoded->respiratory_others; } ?>" >
                                </div>
                            </div>
                        </div>

                        <div class="grid col-md-2">
                            <label class="blocked">Cardiovascular</label>
                            <div class="whiteback">
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="cardiovascular[]" id="cardiovascularAll" value="All Unremarkable"
                                        <?php
                                        if(isset($decoded->cardiovascular))
                                        {
                                            foreach ($decoded->cardiovascular as $key => $value) {
                                                if($value == "All Unremarkable")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                        ?>
                                      > All Unremarkable
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="cardiovascular[]" id="cardiovascular" value="Chest Pains"
                                        <?php
                                        if(isset($decoded->cardiovascular))
                                        {
                                            foreach ($decoded->cardiovascular as $key => $value) {
                                                if($value == "Chest Pains")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                        ?>
                                      > Chest Pains
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="cardiovascular[]" id="cardiovascular" value="Orthopnea"
                                        <?php
                                        if(isset($decoded->cardiovascular))
                                        {
                                            foreach ($decoded->cardiovascular as $key => $value) {
                                                if($value == "Orthopnea")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                        ?>
                                      > Orthopnea
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="cardiovascular[]" id="cardiovascular" value="Palpitations"
                                        <?php
                                        if(isset($decoded->cardiovascular))
                                        {
                                            foreach ($decoded->cardiovascular as $key => $value) {
                                                if($value == "Palpitations")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                        ?>
                                      > Palpitations
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="cardiovascular[]" id="cardiovascular" value="Syncope"
                                        <?php
                                        if(isset($decoded->cardiovascular))
                                        {
                                            foreach ($decoded->cardiovascular as $key => $value) {
                                                if($value == "Syncope")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                        ?>
                                      > Syncope
                                    </label>
                                </div>
                                <label> Others </label>
                                <div class="col-md-12">
                                    <input type="textarea" name="cardiovascular_others" id="cardiovascular" value="<?php if(isset($decoded->cardiovascular_others)) { echo $decoded->cardiovascular_others; } ?>" >
                                </div>
                            </div>
                        </div>

                        <div class="grid col-md-2">
                            <label class="blocked">Gastrointestinal</label>
                            <div class="whiteback">
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="gastrointestinal[]" id="gastrointestinalAll" value="All Unremarkable"
                                        <?php
                                        if(isset($decoded->gastrointestinal))
                                        {
                                            foreach ($decoded->gastrointestinal as $key => $value) {
                                                if($value == "All Unremarkable")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                        ?>
                                      > All Unremarkable
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="gastrointestinal[]" id="gastrointestinal" value="Constipation"
                                        <?php
                                        if(isset($decoded->gastrointestinal))
                                        {
                                            foreach ($decoded->gastrointestinal as $key => $value) {
                                                if($value == "Constipation")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                        ?>
                                      > Constipation
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="gastrointestinal[]" id="gastrointestinal" value="Diarrhea"
                                        <?php
                                        if(isset($decoded->gastrointestinal))
                                        {
                                            foreach ($decoded->gastrointestinal as $key => $value) {
                                                if($value == "Diarrhea")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                        ?>
                                      > Diarrhea
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="gastrointestinal[]" id="gastrointestinal" value="Dysphagia"
                                        <?php
                                        if(isset($decoded->gastrointestinal))
                                        {
                                            foreach ($decoded->gastrointestinal as $key => $value) {
                                                if($value == "Dysphagia")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                        ?>
                                      > Dysphagia
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="gastrointestinal[]" id="gastrointestinal" value="Jaundice"
                                        <?php
                                        if(isset($decoded->gastrointestinal))
                                        {
                                            foreach ($decoded->gastrointestinal as $key => $value) {
                                                if($value == "Jaundice")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                        ?>
                                      > Jaundice
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="gastrointestinal[]" id="gastrointestinal" value="Heartburn"
                                        <?php
                                        if(isset($decoded->gastrointestinal))
                                        {
                                            foreach ($decoded->gastrointestinal as $key => $value) {
                                                if($value == "Heartburn")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                        ?>
                                      > Heartburn
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="gastrointestinal[]" id="gastrointestinal" value="Nausea"
                                        <?php
                                        if(isset($decoded->gastrointestinal))
                                        {
                                            foreach ($decoded->gastrointestinal as $key => $value) {
                                                if($value == "Nausea")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                        ?>
                                      > Nausea
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="gastrointestinal[]" id="gastrointestinal" value="Rectal Bleeding"
                                        <?php
                                        if(isset($decoded->gastrointestinal))
                                        {
                                            foreach ($decoded->gastrointestinal as $key => $value) {
                                                if($value == "Rectal Bleeding")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                        ?>
                                      > Rectal Bleeding
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="gastrointestinal[]" id="gastrointestinal" value="Vomiting"
                                        <?php
                                        if(isset($decoded->gastrointestinal))
                                        {
                                            foreach ($decoded->gastrointestinal as $key => $value) {
                                                if($value == "Vomiting")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                        ?>
                                      > Vomiting
                                    </label>
                                </div>
                                <label> Others </label>
                                <div class="col-md-12">
                                    <input type="textarea" name="gastrointestinal_others" id="gastrointestinal" value="<?php if(isset($decoded->gastrointestinal_others)) { echo $decoded->gastrointestinal_others; } ?>" >
                                </div>
                            </div>
                        </div>

                        <div class="grid col-md-2">
                            <label class="blocked">Endocrine</label>
                            <div class="whiteback">
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="endocrine[]" id="endocrineAll" value="All Unremarkable"
                                        <?php
                                        if(isset($decoded->endocrine))
                                        {
                                            foreach ($decoded->endocrine as $key => $value) {
                                                if($value == "All Unremarkable")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                        ?>
                                      > All Unremarkable
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="endocrine[]" id="endocrine" value="Cold Intolerance"
                                        <?php
                                        if(isset($decoded->endocrine))
                                        {
                                            foreach ($decoded->endocrine as $key => $value) {
                                                if($value == "Cold Intolerance")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                        ?>
                                      > Cold Intolerance
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="endocrine[]" id="endocrine" value="Excessive Sweating"
                                        <?php
                                        if(isset($decoded->endocrine))
                                        {
                                            foreach ($decoded->endocrine as $key => $value) {
                                                if($value == "Excessive Sweating")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                        ?>
                                      > Excessive Sweating
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="endocrine[]" id="endocrine" value="Excessive Thirst"
                                        <?php
                                        if(isset($decoded->endocrine))
                                        {
                                            foreach ($decoded->endocrine as $key => $value) {
                                                if($value == "Excessive Thirst")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                        ?>
                                      > Excessive Thirst
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="endocrine[]" id="endocrine" value="Heat Intolerance"
                                        <?php
                                        if(isset($decoded->endocrine))
                                        {
                                            foreach ($decoded->endocrine as $key => $value) {
                                                if($value == "Heat Intolerance")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                        ?>
                                      > Heat Intolerance
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="endocrine[]" id="endocrine" value="Polyuria"
                                        <?php
                                        if(isset($decoded->endocrine))
                                        {
                                            foreach ($decoded->endocrine as $key => $value) {
                                                if($value == "Polyuria")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                        ?>
                                      > Polyuria
                                    </label>
                                </div>
                                <label> Others </label>
                                <div class="col-md-12">
                                    <input type="textarea" name="endocrine_others" id="endocrine" value="<?php if(isset($decoded->endocrine_others)) { echo $decoded->endocrine_others; } ?>" >
                                </div>
                            </div>
                        </div>

                        <div class="grid col-md-2">
                            <label class="blocked">Genitourinary</label>
                            <div class="whiteback">
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="genitourinary[]" id="genitourinaryAll" value="All Unremarkable"
                                        <?php
                                        if(isset($decoded->genitourinary))
                                        {
                                            foreach ($decoded->genitourinary as $key => $value) {
                                                if($value == "All Unremarkable")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                        ?>
                                      > All Unremarkable
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="genitourinary[]" id="genitourinary" value="Discharge"
                                        <?php
                                        if(isset($decoded->genitourinary))
                                        {
                                            foreach ($decoded->genitourinary as $key => $value) {
                                                if($value == "Discharge")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                        ?>
                                      > Discharge
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="genitourinary[]" id="genitourinary" value="Dysuria"
                                        <?php
                                        if(isset($decoded->genitourinary))
                                        {
                                            foreach ($decoded->genitourinary as $key => $value) {
                                                if($value == "Dysuria")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                        ?>
                                      > Dysuria
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="genitourinary[]" id="genitourinary" value="Sexual Dysfunction"
                                        <?php
                                        if(isset($decoded->genitourinary))
                                        {
                                            foreach ($decoded->genitourinary as $key => $value) {
                                                if($value == "Sexual Dysfunction")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                        ?>
                                      > Sexual Dysfunction
                                    </label>
                                </div>
                                <label> Others </label>
                                <div class="col-md-12">
                                    <input type="textarea" name="genitourinary_others" id="genitourinary" value="<?php if(isset($decoded->genitourinary_others)) { echo $decoded->genitourinary_others; } ?>" >
                                </div>
                            </div>
                        </div>

                        <div class="grid col-md-2">
                            <label class="blocked">Neurological</label>
                            <div class="whiteback">
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="neurological[]" id="neurologicalAll" value="All Unremarkable"
                                        <?php
                                        if(isset($decoded->neurological))
                                        {
                                            foreach ($decoded->neurological as $key => $value) {
                                                if($value == "All Unremarkable")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                        ?>
                                      > All Unremarkable
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="neurological[]" id="neurological" value="Seizures"
                                        <?php
                                        if(isset($decoded->neurological))
                                        {
                                            foreach ($decoded->neurological as $key => $value) {
                                                if($value == "Seizures")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                        ?>
                                      > Seizures
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="neurological[]" id="neurological" value="Tremors"
                                        <?php
                                        if(isset($decoded->neurological))
                                        {
                                            foreach ($decoded->neurological as $key => $value) {
                                                if($value == "Tremors")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        }
                                        ?>
                                      > Tremors
                                    </label>
                                </div>
                                <label> Others </label>
                                <div class="col-md-12">
                                    <input type="textarea" name="neurological_others" id="neurological" value="<?php if(isset($decoded->neurological_others)) { echo $decoded->neurological_others; } ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row show-grid">
                    <div class="header grid col-md-12">Past Medical History</div>
                </div>

                <div class="form-group">
                    <div class="grid col-md-12 blueback"><label>Allergies</label></div>
                    <div class="show-grid">
                        <div class="grid col-md-2">
                            <label>Allergen</label>
                        </div>
                        <div class="grid col-md-2">
                            <label>Date/Age of Diagnosis</label>
                        </div>
                        <div class="grid col-md-2">
                            <label>Reaction</label>
                        </div>
                        <div class="grid col-md-2">
                            <label>Severity</label>
                        </div>
                        <div class="grid col-md-4">
                            <label>Comments</label>
                        </div>
                    </div>

                    <div class="show-grid">
                        <div class="grid col-md-2">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="allergies1[allergen]" id="allergies1" value="<?php if(isset($decoded->allergies1->allergen)) { echo $decoded->allergies1->allergen; } ?>" />
                        </div>
                        <div class="grid col-md-2">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="allergies1[date]" id="allergies1" value="<?php if(isset($decoded->allergies1->date)) { echo $decoded->allergies1->date; } ?>" />
                        </div>
                        <div class="grid col-md-2">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="allergies1[reaction]" id="allergies1" value="<?php if(isset($decoded->allergies1->reaction)) { echo $decoded->allergies1->reaction; } ?>" />
                        </div>
                        <div class="grid col-md-2">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="allergies1[severity]" id="allergies1" value="<?php if(isset($decoded->allergies1->severity)) { echo $decoded->allergies1->severity; } ?>"  />
                        </div>
                        <div class="grid col-md-4">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="allergies1[comments]" id="allergies1" value="<?php if(isset($decoded->allergies1->comments)) { echo $decoded->allergies1->comments; } ?>" />
                        </div>
                    </div>

                    <div class="show-grid">
                        <div class="grid col-md-2">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="allergies2[allergen]" id="allergies2" value="<?php if(isset($decoded->allergies2->allergen)) { echo $decoded->allergies2->allergen; } ?>" />
                        </div>
                        <div class="grid col-md-2">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="allergies2[date]" id="allergies2" value="<?php if(isset($decoded->allergies2->date)) { echo $decoded->allergies2->date; } ?>" />
                        </div>
                        <div class="grid col-md-2">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="allergies2[reaction]" id="allergies2" value="<?php if(isset($decoded->allergies2->reaction)) { echo $decoded->allergies2->reaction; } ?>" />
                        </div>
                        <div class="grid col-md-2">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="allergies2[severity]" id="allergies2" value="<?php if(isset($decoded->allergies2->severity)) { echo $decoded->allergies2->severity; } ?>"  />
                        </div>
                        <div class="grid col-md-4">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="allergies2[comments]" id="allergies2" value="<?php if(isset($decoded->allergies2->comments)) { echo $decoded->allergies2->comments; } ?>" />
                        </div>
                    </div>

                    <div class="show-grid">
                        <div class="grid col-md-2">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="allergies3[allergen]" id="allergies3" value="<?php if(isset($decoded->allergies3->allergen)) { echo $decoded->allergies3->allergen; } ?>" />
                        </div>
                        <div class="grid col-md-2">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="allergies3[date]" id="allergies3" value="<?php if(isset($decoded->allergies3->date)) { echo $decoded->allergies3->date; } ?>" />
                        </div>
                        <div class="grid col-md-2">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="allergies3[reaction]" id="allergies3" value="<?php if(isset($decoded->allergies3->reaction)) { echo $decoded->allergies3->reaction; } ?>" />
                        </div>
                        <div class="grid col-md-2">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="allergies3[severity]" id="allergies3" value="<?php if(isset($decoded->allergies3->severity)) { echo $decoded->allergies3->severity; } ?>"  />
                        </div>
                        <div class="grid col-md-4">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="allergies3[comments]" id="allergies3" value="<?php if(isset($decoded->allergies3->comments)) { echo $decoded->allergies3->comments; } ?>" />
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12 blueback"><label> Previous Medical Diagnoses</label></div>
                    <div class="show-grid">
                        <div class="grid col-md-2">
                            <label>Disease</label>
                        </div>
                        <div class="grid col-md-2">
                            <label>Date of Diagnosis</label>
                        </div>
                        <div class="grid col-md-2">
                            <label>Treatment</label>
                        </div>
                        <div class="grid col-md-2">
                            <label>Test Results</label>
                        </div>
                        <div class="grid col-md-4">
                            <label>Comments</label>
                        </div>
                    </div>

                    <div class="show-grid">
                        <div class="grid col-md-2">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="previous_diagnosis1[disease]" id="previous_diagnosis1" value="<?php if(isset($decoded->previous_diagnosis1->disease)) { echo $decoded->previous_diagnosis1->disease; } ?>" />
                        </div>
                        <div class="grid col-md-2">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="previous_diagnosis1[date]" id="previous_diagnosis1" value="<?php if(isset($decoded->previous_diagnosis1->date)) { echo $decoded->previous_diagnosis1->date; } ?>" />
                        </div>
                        <div class="grid col-md-2">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="previous_diagnosis1[curr_treatment]" id="previous_diagnosis1" value="<?php if(isset($decoded->previous_diagnosis1->curr_treatment)) { echo $decoded->previous_diagnosis1->curr_treatment; } ?>" />
                        </div>
                        <div class="grid col-md-2">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="previous_diagnosis1[test_results]" id="previous_diagnosis1" value="<?php if(isset($decoded->previous_diagnosis1->test_results)) { echo $decoded->previous_diagnosis1->test_results; } ?>" />
                        </div>
                        <div class="grid col-md-4">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="previous_diagnosis1[comments]" id="previous_diagnosis1" value="<?php if(isset($decoded->previous_diagnosis1->comments)) { echo $decoded->previous_diagnosis1->comments; } ?>" />
                        </div>
                    </div>

                    <div class="show-grid">
                        <div class="grid col-md-2">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="previous_diagnosis2[disease]" id="previous_diagnosis2" value="<?php if(isset($decoded->previous_diagnosis2->disease)) { echo $decoded->previous_diagnosis2->disease; } ?>" />
                        </div>
                        <div class="grid col-md-2">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="previous_diagnosis2[date]" id="previous_diagnosis2" value="<?php if(isset($decoded->previous_diagnosis2->date)) { echo $decoded->previous_diagnosis2->date; } ?>" />
                        </div>
                        <div class="grid col-md-2">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="previous_diagnosis2[curr_treatment]" id="previous_diagnosis2" value="<?php if(isset($decoded->previous_diagnosis2->curr_treatment)) { echo $decoded->previous_diagnosis2->curr_treatment; } ?>" />
                        </div>
                        <div class="grid col-md-2">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="previous_diagnosis2[test_results]" id="previous_diagnosis2" value="<?php if(isset($decoded->previous_diagnosis2->test_results)) { echo $decoded->previous_diagnosis2->test_results; } ?>" />
                        </div>
                        <div class="grid col-md-4">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="previous_diagnosis2[comments]" id="previous_diagnosis2" value="<?php if(isset($decoded->previous_diagnosis2->comments)) { echo $decoded->previous_diagnosis2->comments; } ?>" />
                        </div>
                    </div>

                    <div class="show-grid">
                        <div class="grid col-md-2">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="previous_diagnosis3[disease]" id="previous_diagnosis3" value="<?php if(isset($decoded->previous_diagnosis3->disease)) { echo $decoded->previous_diagnosis3->disease; } ?>" />
                        </div>
                        <div class="grid col-md-2">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="previous_diagnosis3[date]" id="previous_diagnosis3" value="<?php if(isset($decoded->previous_diagnosis3->date)) { echo $decoded->previous_diagnosis3->date; } ?>" />
                        </div>
                        <div class="grid col-md-2">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="previous_diagnosis3[curr_treatment]" id="previous_diagnosis3" value="<?php if(isset($decoded->previous_diagnosis3->curr_treatment)) { echo $decoded->previous_diagnosis3->curr_treatment; } ?>" />
                        </div>
                        <div class="grid col-md-2">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="previous_diagnosis3[test_results]" id="previous_diagnosis3" value="<?php if(isset($decoded->previous_diagnosis3->test_results)) { echo $decoded->previous_diagnosis3->test_results; } ?>" />
                        </div>
                        <div class="grid col-md-4">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="previous_diagnosis3[comments]" id="previous_diagnosis3" value="<?php if(isset($decoded->previous_diagnosis3->comments)) { echo $decoded->previous_diagnosis3->comments; } ?>" />
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12 blueback"><label>Previous Surgeries</label></div>
                    <div class="show-grid">
                        <div class="grid col-md-3">
                            <label>Reason for Surgery</label>
                        </div>
                        <div class="grid col-md-2">
                            <label>Date of Surgery</label>
                        </div>
                        <div class="grid col-md-3">
                            <label>Date of Discharge</label>
                        </div>
                        <div class="grid col-md-4">
                            <label>Comments</label>
                        </div>

                    </div>
                    <div class="show-grid">
                        <div class="grid col-md-3">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="previous_surgeries1[reason]" id="previous_surgeries1" value="<?php if(isset($decoded->previous_surgeries1->reason)) { echo $decoded->previous_surgeries1->reason; } ?>"  />
                        </div>
                        <div class="grid col-md-2">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="previous_surgeries1[date_surgery]" id="previous_surgeries1" value="<?php if(isset($decoded->previous_surgeries1->date_surgery)) { echo $decoded->previous_surgeries1->date_surgery; } ?>" />
                        </div>
                        <!-- <div class="grid col-md-2">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="previous_surgeries1[age_surgery]" id="previous_surgeries1" />
                        </div> -->
                        <div class="grid col-md-3">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="previous_surgeries1[date_discharge]" id="previous_surgeries1" value="<?php if(isset($decoded->previous_surgeries1->date_discharge)) { echo $decoded->previous_surgeries1->date_discharge; } ?>" />
                        </div>
                        <div class="grid col-md-4">
                            <input type="text" placeholder="Results, Complications" class="col-md-12 form-input" name="previous_surgeries1[comments]" id="previous_surgeries1" value="<?php if(isset($decoded->previous_surgeries1->comments)) { echo $decoded->previous_surgeries1->comments; } ?>" />
                        </div>
                    </div>
                    <div class="show-grid">
                        <div class="grid col-md-3">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="previous_surgeries2[reason]" id="previous_surgeries2" value="<?php if(isset($decoded->previous_surgeries2->reason)) { echo $decoded->previous_surgeries2->reason; } ?>"  />
                        </div>
                        <div class="grid col-md-2">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="previous_surgeries2[date_surgery]" id="previous_surgeries2" value="<?php if(isset($decoded->previous_surgeries2->date_surgery)) { echo $decoded->previous_surgeries2->date_surgery; } ?>" />
                        </div>
                        <!-- <div class="grid col-md-2">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="previous_surgeries2[age_surgery]" id="previous_surgeries2" />
                        </div> -->
                        <div class="grid col-md-3">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="previous_surgeries2[date_discharge]" id="previous_surgeries2" value="<?php if(isset($decoded->previous_surgeries2->date_discharge)) { echo $decoded->previous_surgeries2->date_discharge; } ?>" />
                        </div>
                        <div class="grid col-md-4">
                            <input type="text" placeholder="Results, Complications" class="col-md-12 form-input" name="previous_surgeries2[comments]" id="previous_surgeries2" value="<?php if(isset($decoded->previous_surgeries2->comments)) { echo $decoded->previous_surgeries2->comments; } ?>" />
                        </div>
                    </div>
                    <div class="show-grid">
                        <div class="grid col-md-3">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="previous_surgeries3[reason]" id="previous_surgeries3" value="<?php if(isset($decoded->previous_surgeries3->reason)) { echo $decoded->previous_surgeries3->reason; } ?>"  />
                        </div>
                        <div class="grid col-md-2">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="previous_surgeries3[date_surgery]" id="previous_surgeries3" value="<?php if(isset($decoded->previous_surgeries3->date_surgery)) { echo $decoded->previous_surgeries3->date_surgery; } ?>" />
                        </div>
                        <!-- <div class="grid col-md-2">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="previous_surgeries3[age_surgery]" id="previous_surgeries3" />
                        </div> -->
                        <div class="grid col-md-3">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="previous_surgeries3[date_discharge]" id="previous_surgeries3" value="<?php if(isset($decoded->previous_surgeries3->date_discharge)) { echo $decoded->previous_surgeries3->date_discharge; } ?>" />
                        </div>
                        <div class="grid col-md-4">
                            <input type="text" placeholder="Results, Complications" class="col-md-12 form-input" name="previous_surgeries3[comments]" id="previous_surgeries3" value="<?php if(isset($decoded->previous_surgeries3->comments)) { echo $decoded->previous_surgeries3->comments; } ?>" />
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12 blueback"><label>Previous Hospitalizations</label></div>
                    <div class="show-grid">
                         <div class="grid col-md-4">
                            <label>Reason for Admission</label>
                        </div>
                       <div class="grid col-md-3">
                            <label>Date of Admission</label>
                        </div>
                        <div class="grid col-md-5">
                            <label>Comments</label>
                        </div>

                    </div>
                    <div class="show-grid">
                        <div class="grid col-md-4">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="previous_hospitalization1[reason]" id="previous_hospitalization1" value="<?php if(isset($decoded->previous_hospitalization1->reason)) { echo $decoded->previous_hospitalization1->reason; } ?>" />
                        </div>
                        <div class="grid col-md-3">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="previous_hospitalization1[date_admission]" id="previous_hospitalization1" value="<?php if(isset($decoded->previous_hospitalization1->date_admission)) { echo $decoded->previous_hospitalization1->date_admission; } ?>" />
                        </div>
                        <div class="grid col-md-5">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="previous_hospitalization1[comments]" id="previous_hospitalization1" value="<?php if(isset($decoded->previous_hospitalization1->comments)) { echo $decoded->previous_hospitalization1->comments; } ?>" />
                        </div>
                    </div>
                    <div class="show-grid">
                        <div class="grid col-md-4">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="previous_hospitalization2[reason]" id="previous_hospitalization2" value="<?php if(isset($decoded->previous_hospitalization2->reason)) { echo $decoded->previous_hospitalization2->reason; } ?>" />
                        </div>
                        <div class="grid col-md-3">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="previous_hospitalization2[date_admission]" id="previous_hospitalization2" value="<?php if(isset($decoded->previous_hospitalization2->date_admission)) { echo $decoded->previous_hospitalization2->date_admission; } ?>" />
                        </div>
                        <div class="grid col-md-5">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="previous_hospitalization2[comments]" id="previous_hospitalization2" value="<?php if(isset($decoded->previous_hospitalization2->comments)) { echo $decoded->previous_hospitalization2->comments; } ?>" />
                        </div>
                    </div>
                    <div class="show-grid">
                        <div class="grid col-md-4">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="previous_hospitalization3[reason]" id="previous_hospitalization3" value="<?php if(isset($decoded->previous_hospitalization3->reason)) { echo $decoded->previous_hospitalization3->reason; } ?>" />
                        </div>
                        <div class="grid col-md-3">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="previous_hospitalization3[date_admission]" id="previous_hospitalization3" value="<?php if(isset($decoded->previous_hospitalization3->date_admission)) { echo $decoded->previous_hospitalization3->date_admission; } ?>" />
                        </div>
                        <div class="grid col-md-5">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="previous_hospitalization3[comments]" id="previous_hospitalization3" value="<?php if(isset($decoded->previous_hospitalization3->comments)) { echo $decoded->previous_hospitalization3->comments; } ?>" />
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12 blueback"><label>Current Medications (Including Herbals, Supplements, Alternative Therapy)</label></div>
                    <div class="show-grid">
                        <div class="grid col-md-3">
                            <label>Medications Currently Being Taken</label>
                        </div>
                        <div class="grid col-md-2">
                            <label>Dose</label>
                        </div>
                        <div class="grid col-md-3">
                            <label>Indication</label>
                        </div>
                        <div class="grid col-md-4">
                            <label>Comments</label>
                        </div>

                    </div>
                    <div class="show-grid">
                        <div class="grid col-md-3">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="other_medications1[generic_name]" id="other_medications1" value="<?php if(isset($decoded->other_medications1->generic_name)) { echo $decoded->other_medications1->generic_name; } ?>" />
                        </div>
                        <div class="grid col-md-2">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="other_medications1[drug_dosage]" id="other_medications1" value="<?php if(isset($decoded->other_medications1->drug_dosage)) { echo $decoded->other_medications1->drug_dosage; } ?>" />
                        </div>
                        <div class="grid col-md-3">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="other_medications1[indication]" id="other_medications1" value="<?php if(isset($decoded->other_medications1->indication)) { echo $decoded->other_medications1->indication; } ?>" />
                        </div>
                        <div class="grid col-md-4">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="other_medications1[comments]" id="other_medications1" value="<?php if(isset($decoded->other_medications1->comments)) { echo $decoded->other_medications1->comments; } ?>" />
                        </div>
                    </div>
                    <div class="show-grid">
                        <div class="grid col-md-3">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="other_medications2[generic_name]" id="other_medications2" value="<?php if(isset($decoded->other_medications2->generic_name)) { echo $decoded->other_medications2->generic_name; } ?>" />
                        </div>
                        <div class="grid col-md-2">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="other_medications2[drug_dosage]" id="other_medications2" value="<?php if(isset($decoded->other_medications2->drug_dosage)) { echo $decoded->other_medications2->drug_dosage; } ?>" />
                        </div>
                        <div class="grid col-md-3">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="other_medications2[indication]" id="other_medications2" value="<?php if(isset($decoded->other_medications2->indication)) { echo $decoded->other_medications2->indication; } ?>" />
                        </div>
                        <div class="grid col-md-4">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="other_medications2[comments]" id="other_medications2" value="<?php if(isset($decoded->other_medications2->comments)) { echo $decoded->other_medications2->comments; } ?>" />
                        </div>
                    </div>
                    <div class="show-grid">
                        <div class="grid col-md-3">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="other_medications3[generic_name]" id="other_medications3" value="<?php if(isset($decoded->other_medications3->generic_name)) { echo $decoded->other_medications3->generic_name; } ?>" />
                        </div>
                        <div class="grid col-md-2">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="other_medications3[drug_dosage]" id="other_medications3" value="<?php if(isset($decoded->other_medications3->drug_dosage)) { echo $decoded->other_medications3->drug_dosage; } ?>" />
                        </div>
                        <div class="grid col-md-3">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="other_medications3[indication]" id="other_medications3" value="<?php if(isset($decoded->other_medications3->indication)) { echo $decoded->other_medications3->indication; } ?>" />
                        </div>
                        <div class="grid col-md-4">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="other_medications3[comments]" id="other_medications3" value="<?php if(isset($decoded->other_medications3->comments)) { echo $decoded->other_medications3->comments; } ?>" />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12 blueback"><label>Present Lab / Imaging / Ancillary Test Results </label></div>
                    <div class="show-grid">
                        <div class="grid col-md-2">
                            <label>Date</label>
                        </div>
                        <div class="grid col-md-3">
                            <label>Test</label>
                        </div>
                        <div class="grid col-md-3">
                            <label>Result</label>
                        </div>
                        <!-- <div class="grid col-md-2">
                            <label>Inclusive Dates</label>
                        </div> -->
                        <div class="grid col-md-4">
                            <label>Comments</label>
                        </div>

                    </div>
                    <div class="show-grid">
                        <div class="grid col-md-2">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="lab_results1[date]" id="lab_results1" value="<?php if(isset($decoded->lab_results1->date)) { echo $decoded->lab_results1->date; } ?>" />
                        </div>
                        <div class="grid col-md-3">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="lab_results1[test]" id="lab_results1" value="<?php if(isset($decoded->lab_results1->test)) { echo $decoded->lab_results1->test; } ?>" />
                        </div>
                        <div class="grid col-md-3">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="lab_results1[result]" id="lab_results1" value="<?php if(isset($decoded->lab_results1->result)) { echo $decoded->lab_results1->result; } ?>" />
                        </div>
                        <div class="grid col-md-4">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="lab_results1[comments]" id="lab_reuslts1" value="<?php if(isset($decoded->lab_results1->comments)) { echo $decoded->lab_results1->comments; } ?>" />
                        </div>
                    </div>
                    <div class="show-grid">
                        <div class="grid col-md-2">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="lab_results2[date]" id="lab_results2" value="<?php if(isset($decoded->lab_results2->date)) { echo $decoded->lab_results2->date; } ?>" />
                        </div>
                        <div class="grid col-md-3">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="lab_results2[test]" id="lab_results2" value="<?php if(isset($decoded->lab_results2->test)) { echo $decoded->lab_results2->test; } ?>" />
                        </div>
                        <div class="grid col-md-3">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="lab_results2[result]" id="lab_results2" value="<?php if(isset($decoded->lab_results2->result)) { echo $decoded->lab_results2->result; } ?>" />
                        </div>
                        <div class="grid col-md-4">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="lab_results2[comments]" id="lab_reuslts1" value="<?php if(isset($decoded->lab_results2->comments)) { echo $decoded->lab_results2->comments; } ?>" />
                        </div>
                    </div>
                    <div class="show-grid">
                        <div class="grid col-md-2">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="lab_results3[date]" id="lab_results3" value="<?php if(isset($decoded->lab_results3->date)) { echo $decoded->lab_results3->date; } ?>" />
                        </div>
                        <div class="grid col-md-3">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="lab_results3[test]" id="lab_results3" value="<?php if(isset($decoded->lab_results3->test)) { echo $decoded->lab_results3->test; } ?>" />
                        </div>
                        <div class="grid col-md-3">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="lab_results3[result]" id="lab_results3" value="<?php if(isset($decoded->lab_results3->result)) { echo $decoded->lab_results3->result; } ?>" />
                        </div>
                        <div class="grid col-md-4">
                            <input type="text" placeholder="" class="col-md-12 form-input" name="lab_results3[comments]" id="lab_reuslts1" value="<?php if(isset($decoded->lab_results3->comments)) { echo $decoded->lab_results3->comments; } ?>" />
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row show-grid">
                    <div class="header col-md-12">Family History</div>
                </div>
                <div class="row show-grid">
                    <div class="grid col-md-2">
                        <label>Relative</label>
                    </div>
                    <div class="grid col-md-4">
                        <label>Known Illnesses</label>
                    </div>
                    <div class="grid col-md-2">
                        <label>Age of Diagnosis</label>
                    </div>
                    <div class="grid col-md-4">
                        <label>Comments</label>
                    </div>
                </div>
                <div class="row show-grid">
                    <div class="grid col-md-2">
                        <input type="text" placeholder="Relationship" class="col-md-12 form-input" name="family_history1[name]" id="family_history1" value="<?php if(isset($decoded->family_history1->name)) { echo $decoded->family_history1->name; } ?>"   />
                    </div>
                    <div class="grid col-md-4">
                        <input type="text" placeholder="Diagnosis 1, Diagnosis 2..." class="col-md-12 form-input" name="family_history1[diagnosis]" id="family_history1" value="<?php if(isset($decoded->family_history1->diagnosis)) { echo $decoded->family_history1->diagnosis; } ?>" />
                    </div>
                    <div class="grid col-md-2">
                        <input type="text" placeholder="Age" class="col-md-12 form-input" name="family_history1[age_diagnosis]" id="family_history1" value="<?php if(isset($decoded->family_history1->age_diagnosis)) { echo $decoded->family_history1->age_diagnosis; } ?>" />
                    </div>
                    <div class="grid col-md-4">
                        <input type="text" placeholder="Comments" class="col-md-12 form-input" name="family_history1[comments]" id="family_history1" value="<?php if(isset($decoded->family_history1->comments)) { echo $decoded->family_history1->comments; } ?>" />
                    </div>
                </div>
                <div class="row show-grid">
                    <div class="grid col-md-2">
                        <input type="text" placeholder="Relationship" class="col-md-12 form-input" name="family_history2[name]" id="family_history2" value="<?php if(isset($decoded->family_history2->name)) { echo $decoded->family_history2->name; } ?>"   />
                    </div>
                    <div class="grid col-md-4">
                        <input type="text" placeholder="Diagnosis 1, Diagnosis 2..." class="col-md-12 form-input" name="family_history2[diagnosis]" id="family_history2" value="<?php if(isset($decoded->family_history2->diagnosis)) { echo $decoded->family_history2->diagnosis; } ?>" />
                    </div>
                    <div class="grid col-md-2">
                        <input type="text" placeholder="Age" class="col-md-12 form-input" name="family_history2[age_diagnosis]" id="family_history2" value="<?php if(isset($decoded->family_history2->age_diagnosis)) { echo $decoded->family_history2->age_diagnosis; } ?>" />
                    </div>
                    <div class="grid col-md-4">
                        <input type="text" placeholder="Comments" class="col-md-12 form-input" name="family_history2[comments]" id="family_history2" value="<?php if(isset($decoded->family_history2->comments)) { echo $decoded->family_history2->comments; } ?>" />
                    </div>
                </div>
                <div class="row show-grid">
                    <div class="grid col-md-2">
                        <input type="text" placeholder="Relationship" class="col-md-12 form-input" name="family_history3[name]" id="family_history3" value="<?php if(isset($decoded->family_history3->name)) { echo $decoded->family_history3->name; } ?>"   />
                    </div>
                    <div class="grid col-md-4">
                        <input type="text" placeholder="Diagnosis 1, Diagnosis 2..." class="col-md-12 form-input" name="family_history3[diagnosis]" id="family_history3" value="<?php if(isset($decoded->family_history3->diagnosis)) { echo $decoded->family_history3->diagnosis; } ?>" />
                    </div>
                    <div class="grid col-md-2">
                        <input type="text" placeholder="Age" class="col-md-12 form-input" name="family_history3[age_diagnosis]" id="family_history3" value="<?php if(isset($decoded->family_history3->age_diagnosis)) { echo $decoded->family_history3->age_diagnosis; } ?>" />
                    </div>
                    <div class="grid col-md-4">
                        <input type="text" placeholder="Comments" class="col-md-12 form-input" name="family_history3[comments]" id="family_history3" value="<?php if(isset($decoded->family_history3->comments)) { echo $decoded->family_history3->comments; } ?>" />
                    </div>
                </div>
                <div class="row show-grid">
                    <div class="grid col-md-2">
                        <input type="text" placeholder="Relationship" class="col-md-12 form-input" name="family_history4[name]" id="family_history4" value="<?php if(isset($decoded->family_history4->name)) { echo $decoded->family_history4->name; } ?>"   />
                    </div>
                    <div class="grid col-md-4">
                        <input type="text" placeholder="Diagnosis 1, Diagnosis 2..." class="col-md-12 form-input" name="family_history4[diagnosis]" id="family_history4" value="<?php if(isset($decoded->family_history4->diagnosis)) { echo $decoded->family_history4->diagnosis; } ?>" />
                    </div>
                    <div class="grid col-md-2">
                        <input type="text" placeholder="Age" class="col-md-12 form-input" name="family_history4[age_diagnosis]" id="family_history4" value="<?php if(isset($decoded->family_history4->age_diagnosis)) { echo $decoded->family_history4->age_diagnosis; } ?>" />
                    </div>
                    <div class="grid col-md-4">
                        <input type="text" placeholder="Comments" class="col-md-12 form-input" name="family_history4[comments]" id="family_history4" value="<?php if(isset($decoded->family_history4->comments)) { echo $decoded->family_history4->comments; } ?>" />
                    </div>
                </div>
                <div class="row show-grid">
                    <div class="grid col-md-2">
                        <input type="text" placeholder="Relationship" class="col-md-12 form-input" name="family_history5[name]" id="family_history5" value="<?php if(isset($decoded->family_history5->name)) { echo $decoded->family_history5->name; } ?>"   />
                    </div>
                    <div class="grid col-md-4">
                        <input type="text" placeholder="Diagnosis 1, Diagnosis 2..." class="col-md-12 form-input" name="family_history5[diagnosis]" id="family_history5" value="<?php if(isset($decoded->family_history5->diagnosis)) { echo $decoded->family_history5->diagnosis; } ?>" />
                    </div>
                    <div class="grid col-md-2">
                        <input type="text" placeholder="Age" class="col-md-12 form-input" name="family_history5[age_diagnosis]" id="family_history5" value="<?php if(isset($decoded->family_history5->age_diagnosis)) { echo $decoded->family_history5->age_diagnosis; } ?>" />
                    </div>
                    <div class="grid col-md-4">
                        <input type="text" placeholder="Comments" class="col-md-12 form-input" name="family_history5[comments]" id="family_history5" value="<?php if(isset($decoded->family_history5->comments)) { echo $decoded->family_history5->comments; } ?>" />
                    </div>
                </div>
            </div>


            <div class="form-group">
                <div class="row show-grid">
                    <div class="header col-md-12">Immunization</div>
                </div>
                <div class="row show-grid">
                    <div class="grid col-md-6 whiteback">
                        <div class="checkbox-inline">
                            <label>
                              <input type="checkbox" name="vacc[med_doctor]" id="vacc_med_doctor" <?php if(isset($decoded->vacc->med_doctor) && $decoded->vacc->med_doctor == "on") {echo "checked";} ?> > Vaccinated by Medical Doctor
                            </label>
                        </div>
                    </div>

                    <div class="grid col-md-6 whiteback">
                        <div class="checkbox-inline">
                            <label>
                              <input type="checkbox" name="vacc[health_center]" id="vacc_health_center" <?php if(isset($decoded->vacc->health_center) && $decoded->vacc->health_center == "on") {echo "checked";} ?> > Vaccinated at Health Center
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row show-grid">
                    <div class="grid col-md-12 whiteback">
                        <div class="checkbox-inline">
                            <label>
                              <input type="checkbox" name="vacc[unknown]" id="vacc_unknown" <?php if(isset($decoded->vacc->unknown) && $decoded->vacc->unknown == "on") {echo "checked";} ?> > Unknown, but recalled to be:
                            </label>
                        </div>
                        <div class="radio-inline">
                            <label>
                              <input type="radio" name="vacc[unknown_val]" id="vacc_unknown_val" value="Complete" <?php if(isset($decoded->vacc->unknown_val) && $decoded->vacc->unknown_val == "Complete") {echo "checked";} ?>  disabled> Complete
                            </label>
                        </div>
                        <div class="radio-inline">
                            <label>
                              <input type="radio" name="vacc[unknown_val]" id="vacc_unknown_val" value="Incomplete" <?php if(isset($decoded->vacc->unknown_val) && $decoded->vacc->unknown_val == "Incomplete") {echo "checked";} ?> disabled> Incomplete
                            </label>
                        </div>
                        <div class="radio-inline">
                            <label>
                              <input type="radio" name="vacc[unknown_val]" id="vacc_unknown_val" value="No Vaccinations" <?php if(isset($decoded->vacc->unknown_val) && $decoded->vacc->unknown_val == "No Vaccinations") {echo "checked";} ?> disabled> No Vaccinations
                            </label>
                        </div>
                    </div>             
                </div>
                <div class="row show-grid">
                    <div class="grid col-md-4 textleft">
                        <label>Bacillus Calmette-Guérin (BCG)</label>
                    </div>
                    <div class="grid col-md-4">
                        <input type="text" class="col-md-12 form-input" name="vacc[bcg]" id="vacc_bcg" placeholder="# Taken" value="<?php if(isset($decoded->vacc->bcg)) { echo $decoded->vacc->bcg; } ?>" >
                    </div>
                    <div class="grid col-md-4 textleft">
                        <label> / 1 </label>
                    </div>
                    <div class="grid col-md-4 textleft">
                        <label>Hepatitis B (Hep B)</label>
                    </div>
                    <div class="grid col-md-4">
                        <input type="text" class="col-md-12 form-input" name="vacc[hepB]" id="vacc_hepB" placeholder="# Taken" value="<?php if(isset($decoded->vacc->hepB)) { echo $decoded->vacc->hepB; } ?>">
                    </div>
                    <div class="grid col-md-4 textleft">
                        <label> / 3 </label>
                    </div>
                    <div class="grid col-md-4 textleft">
                        <label>Diphtheria, Tetanus, Pertussis (DTP)</label>
                    </div>
                    <div class="grid col-md-4">
                        <input type="text" class="col-md-12 form-input" name="vacc[dtp]" id="vacc_dtp" placeholder="# Taken" value="<?php if(isset($decoded->vacc->dtp)) { echo $decoded->vacc->dtp; } ?>">
                    </div>
                    <div class="grid col-md-4 textleft">
                        <label> / 5 </label>
                    </div>
                    <div class="grid col-md-4 textleft">
                        <label>Haemophilus Influenzae Type B (HIB)</label>
                    </div>
                    <div class="grid col-md-4">
                        <input type="text" class="col-md-12 form-input" name="vacc[HiB]" id="vacc_HiB" placeholder="# Taken" value="<?php if(isset($decoded->vacc->HiB)) { echo $decoded->vacc->HiB; } ?>">
                    </div>
                    <div class="grid col-md-4 textleft">
                        <label> / 4 </label>
                    </div>
                    <div class="grid col-md-4 textleft">
                        <label>Oral Polio/Inactivated Polio Vaccine (OPV/IPV)</label>
                    </div>
                    <div class="grid col-md-4">
                        <input type="text" class="col-md-12 form-input" name="vacc[ipv]" id="vacc_ipv" placeholder="# Taken" value="<?php if(isset($decoded->vacc->ipv)) { echo $decoded->vacc->ipv; } ?>">
                    </div>
                    <div class="grid col-md-4 textleft">
                        <label> / 4 </label>
                    </div>
                    <div class="grid col-md-4 textleft">
                        <label>Rotavirus Vaccine (RV)</label>
                    </div>
                    <div class="grid col-md-4">
                        <input type="text" class="col-md-12 form-input" name="vacc[rv]" id="vacc_rv" placeholder="# Taken" value="<?php if(isset($decoded->vacc->rv)) { echo $decoded->vacc->rv; } ?>">
                    </div>
                    <div class="grid col-md-4 textleft">
                        <label> / 3 </label>
                    </div>
                    <div class="grid col-md-4 textleft">
                        <label>Pneumococcal Conjugate Vaccine (PCV)</label>
                    </div>
                    <div class="grid col-md-4">
                        <input type="text" class="col-md-12 form-input" name="vacc[pcv]" id="vacc_pcv" placeholder="# Taken" value="<?php if(isset($decoded->vacc->pcv)) { echo $decoded->vacc->pcv; } ?>">
                    </div>
                    <div class="grid col-md-4 textleft">
                        <label> / 4 </label>
                    </div>
                    <div class="grid col-md-4 textleft">
                        <label>Measles, Mumps, and Rubella (MMR)</label>
                    </div>
                    <div class="grid col-md-4">
                        <input type="text" class="col-md-12 form-input" name="vacc[mmr]" id="vacc_mmr" placeholder="# Taken" value="<?php if(isset($decoded->vacc->mmr)) { echo $decoded->vacc->mmr; } ?>">
                    </div>
                    <div class="grid col-md-4 textleft">
                        <label> / 2 </label>
                    </div>
                    <div class="grid col-md-4 textleft">
                        <label>Others</label>
                    </div>
                    <div class="grid col-md-8">
                        <input type="text" class="col-md-12 form-input" name="vacc[others]" id="vacc_others" value="<?php if(isset($decoded->vacc->others)) { echo $decoded->vacc->others; } ?>">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row show-grid">
                    <div class="header col-md-12">Personal and Social History</div>
                </div>
                <div class="row show-grid">
                    <div class="grid col-md-3 textleft">
                        <label>Smoking</label>
                    </div>
                    <div class="grid col-md-9">
                        <input type="text" placeholder="Pack-years" class="col-md-12 form-input" name="personal_history[smoking]" id="ph_smoking" value="<?php if(isset($decoded->personal_history->smoking)) { echo $decoded->personal_history->smoking; } ?>" />
                    </div>
                    <div class="grid col-md-3 textleft">
                        <label>Alcohol Intake</label>
                    </div>
                    <div class="grid col-md-9">
                        <input type="text" placeholder="" class="col-md-12 form-input" name="personal_history[alcohol_intake]" id="ph_alcohol_intake" value="<?php if(isset($decoded->personal_history->alcohol_intake)) { echo $decoded->personal_history->alcohol_intake; } ?>" />
                    </div>
                    <div class="grid col-md-3 textleft">
                        <label>Illicit Substance Use</label>
                    </div>
                    <div class="grid col-md-9">
                        <input type="text" placeholder="" class="col-md-12 form-input" name="personal_history[substance_use]" id="ph_substance_use" value="<?php if(isset($decoded->personal_history->substance_use)) { echo $decoded->personal_history->substance_use; } ?>" />
                    </div>                    
                    <div class="grid col-md-3 textleft">
                        <label>Education</label>
                    </div>
                    <div class="grid col-md-9">
                        <input type="text" placeholder="Highest educational attainment" class="col-md-12 form-input" name="personal_history[education]" id="ph_history" value="<?php if(isset($decoded->personal_history->education)) { echo $decoded->personal_history->education; } ?>" />
                    </div>
                    <div class="grid col-md-3 textleft">
                        <label>Occupation</label>
                    </div>
                    <div class="grid col-md-9">
                        <input type="text" placeholder="" class="col-md-12 form-input" name="personal_history[occupation]" id="ph_occupation" value="<?php if(isset($decoded->personal_history->occupation)) { echo $decoded->personal_history->occupation; } ?>" />
                    </div>
                    <div class="grid col-md-3 textleft">
                        <label>Diet and Physical Activity</label>
                    </div>
                    <div class="grid col-md-9">
                        <input type="text" placeholder="" class="col-md-12 form-input" name="personal_history[exercise]" id="ph_exercise" value="<?php if(isset($decoded->personal_history->exercise)) { echo $decoded->personal_history->exercise; } ?>" />
                    </div>

                    <div class="grid col-md-3 textleft">
                        <label>Others</label>
                    </div>
                    <div class="grid col-md-9">
                        <input type="text" placeholder="" class="col-md-12 form-input" name="personal_history[others]" id="ph_others" value="<?php if(isset($decoded->personal_history->others)) { echo $decoded->personal_history->others; } ?>" />
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row show-grid">
                    <div class="header col-md-12"> Environmental and Travel History</div>
                </div>
                <div class="row show-grid">
                    <div class="grid col-md-3 textleft">
                        <label>Number of Household Members</label>
                    </div>
                    <div class="grid col-md-9">
                        <input type="text" placeholder="" class="col-md-12 form-input" name="eth[no_household_mem]" id="eth_household_mems" value="<?php if(isset($decoded->eth->no_household_mem)) { echo $decoded->eth->no_household_mem; } ?>" />
                    </div>                
                    <div class="grid col-md-3 textleft">
                        <label>Type of Dwelling</label>
                    </div>
                    <div class="grid col-md-9">
                        <input type="text" placeholder="" class="col-md-12 form-input" name="eth[type_dwelling]" id="eth_type_dwelling" value="<?php if(isset($decoded->eth->type_dwelling)) { echo $decoded->eth->type_dwelling; } ?>" />
                    </div>
                    <div class="grid col-md-3 textleft">
                        <label>Drinking Water Source</label>
                    </div>
                    <div class="grid col-md-9">
                        <input type="text" placeholder="" class="col-md-12 form-input" name="eth[drink_water_source]" id="eth_dw_source" value="<?php if(isset($decoded->eth->drink_water_source)) { echo $decoded->eth->drink_water_source; } ?>" />
                    </div>
                    <div class="grid col-md-3 textleft">
                        <label>Electricity</label>
                    </div>
                    <div class="grid col-md-9">
                        <input type="text" placeholder="" class="col-md-12 form-input" name="eth[electricity]" id="eth_electricity" value="<?php if(isset($decoded->eth->electricity)) { echo $decoded->eth->electricity; } ?>" />
                    </div>
                    <div class="grid col-md-3 textleft">
                        <label>Waste and Garbage Disposal</label>
                    </div>
                    <div class="grid col-md-9">
                        <input type="text" placeholder="" class="col-md-12 form-input" name="eth[waste_disposal]" id="eth_waste_disposal" value="<?php if(isset($decoded->eth->waste_disposal)) { echo $decoded->eth->waste_disposal; } ?>" />
                    </div>
                    <div class="grid col-md-3 textleft">
                        <label>Exposure to Fumes/Toxins/Biohazards</label>
                    </div>
                    <div class="grid col-md-9">
                        <input type="text" placeholder="" class="col-md-12 form-input" name="eth[tobacco]" id="eth_tobacco" value="<?php if(isset($decoded->eth->tobacco)) { echo $decoded->eth->tobacco; } ?>" />
                    </div>
                    <div class="grid col-md-3 textleft">
                        <label>Pertinent History of Travel</label>
                    </div>
                    <div class="grid col-md-9">
                        <input type="text" placeholder="" class="col-md-12 form-input" name="eth[history_travel]" id="eth_history_travel" value="<?php if(isset($decoded->eth->history_travel)) { echo $decoded->eth->history_travel; } ?>" />
                    </div>
                    <div class="grid col-md-3 textleft">
                        <label>Others</label>
                    </div>
                    <div class="grid col-md-9">
                        <input type="text" placeholder="" class="col-md-12 form-input" name="eth[others]" id="eth_others" value="<?php if(isset($decoded->eth->others)) { echo $decoded->eth->others; } ?>" />
                    </div>

                </div>
            </div>

            <div class="form-group">
                <div class="row show-grid">
                    <div class="header grid col-md-12">Gynecological History</div>
                </div>
                <div class="row show-grid">
                    <div class="grid col-md-6">
                        <div class="grid col-md-6 textleft">
                            <label>Last Menstrual Period (LMP)</label>
                        </div>
                        <div class="grid col-md-6">
                            <input type="text" placeholder="LMP" class="datepicker col-md-12 form-input" name="ogh[lmp]" id="ogh_lmp" value="<?php if(isset($decoded->ogh->lmp)) { echo $decoded->ogh->lmp; } ?>" />
                        </div>
                        <div class="grid col-md-6 textleft">
                            <label>Previous Menstrual Period (PMP)</label>
                        </div>
                        <div class="grid col-md-6">
                            <input type="text" placeholder="PMP" class="datepicker col-md-12 form-input" name="ogh[pmp]" id="ogh_pmp" value="<?php if(isset($decoded->ogh->pmp)) { echo $decoded->ogh->pmp; } ?>" />
                        </div>
                        <div class="grid col-md-6 textleft">
                            <label>Estimated Date of Confinement (EDC)</label>
                        </div>
                        <div class="grid col-md-6">
                            <input type="text" placeholder="EDC" class="datepicker col-md-12 form-input" name="ogh[edc]" id="ogh_edc" value="<?php if(isset($decoded->ogh->edc)) { echo $decoded->ogh->edc; } ?>" />
                        </div>
                        <div class="grid col-md-6 textleft">
                            <label>Menarche</label>
                        </div>
                        <div class="grid col-md-6">
                            <input type="text" placeholder="Age, year" class="col-md-12 form-input" name="ogh[menarche]" id="ogh_menarche" value="<?php if(isset($decoded->ogh->menarche)) { echo $decoded->ogh->menarche; } ?>" />
                        </div>
                    </div>
                    <div class="grid col-md-6">
                        <div class="grid col-md-6 textleft">
                            <label>Interval</label>
                        </div>
                        <div class="grid col-md-6">
                             <select class="col-md-12 form-input" name="ogh[interval]" id="ogh_interval">
                                <option value="" > -- Select Interval -- </option>
                                <option value="Regular" <?php if(isset($decoded->ogh->interval) && $decoded->ogh->interval == 'Regular') {echo "selected";} ?> >Regular</option>
                                <option value="Irregular" <?php if(isset($decoded->ogh->interval) && $decoded->ogh->interval == 'Irregular') {echo "selected";} ?> >Irregular</option>
                            </select>
                        </div>
                        <div class="grid col-md-6 textleft">
                            <label>Duration </label>
                        </div>
                        <div class="grid col-md-6">
                            <input type="text" placeholder="In days" class="col-md-12 form-input" name="ogh[duration]" id="ogh_duration" value="<?php if(isset($decoded->ogh->duration)) { echo $decoded->ogh->duration; } ?>" />
                        </div>
                        <div class="grid col-md-6 textleft">
                            <label>Amount </label>
                        </div>
                        <div class="grid col-md-6">
                            <input type="text" placeholder="# of soak pads" class="col-md-12 form-input" name="ogh[amount]" id="ogh_amount" value="<?php if(isset($decoded->ogh->amount)) { echo $decoded->ogh->amount; } ?>" />
                        </div>
                        <div class="grid col-md-6 textleft">
                            <label>Symptoms</label>
                        </div>
                        <div class="grid col-md-6">
                            <input type="text" placeholder="Dysmenoroghea, etc..." class="col-md-12 form-input" name="ogh[symptoms]" id="ogh_symptoms" value="<?php if(isset($decoded->ogh->symptoms)) { echo $decoded->ogh->symptoms; } ?>" />
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row show-grid">
                    <div class="header col-md-12">Obstetric History</div>
                </div>
                <div class="row show-grid">
                    <div class="col-md-2">
                        <label>Gravida</label>
                        <input type="text" class="col-md-6" name="ogh[gravida]" id="ogh_gravida" value="<?php if(isset($decoded->ogh->gravida)) { echo $decoded->ogh->gravida; } ?>"> 
                    </div>
                    <div class="col-md-2">
                        <label>Para</label>
                        <input type="text" class="col-md-6" name="ogh[para]" id="ogh_para" value="<?php if(isset($decoded->ogh->para)) { echo $decoded->ogh->para; } ?>"> 
                    </div>
                    <div class="col-md-2">
                        <label>( Term</label>
                        <input type="text" class="col-md-6" name="ogh[term]" id="ogh_term" value="<?php if(isset($decoded->ogh->term)) { echo $decoded->ogh->term; } ?>"> 
                    </div>
                    <div class="col-md-2">
                        <label>Preterm</label>
                        <input type="text" class="col-md-6" name="ogh[preterm]" id="ogh_preterm" value="<?php if(isset($decoded->ogh->preterm)) { echo $decoded->ogh->preterm; } ?>"> 
                    </div>
                    <div class="col-md-2">
                        <label>Abortion</label>
                        <input type="text" class="col-md-6" name="ogh[abortion]" id="ogh_abortion" value="<?php if(isset($decoded->ogh->abortion)) { echo $decoded->ogh->abortion; } ?>"> 
                    </div>
                    <div class="col-md-2">
                        <label>Living</label>
                        <input type="text" class="col-md-6" name="ogh[living]" id="ogh_living" value="<?php if(isset($decoded->ogh->living)) { echo $decoded->ogh->living; } ?>"> 
                        <label> ) </label>
                    </div>
                </div>
                <div class="row show-grid">
                    <div class="grid col-md-1 textleft">
                        <label>Gravida</label>
                    </div>
                    <div class="grid col-md-1 textleft">
                        <label>Date</label>
                    </div>
                    <div class="grid col-md-2 textleft">
                        <label>Type of Delivery</label>
                    </div>
                    <div class="grid col-md-1 textleft">
                        <label>AOG</label>
                    </div>
                    <div class="grid col-md-1 textleft">
                        <label>Sex</label>
                    </div>
                    <div class="grid col-md-2 textleft">
                        <label>Birthplace / Attended By</label>
                    </div>
                    <div class="grid col-md-2 textleft">
                        <label>Birthweight (Grams)</label>
                    </div>
                    <div class="grid col-md-2 textleft">
                        <label>Remarks / Complications</label>
                    </div>
                </div>
                <div class="row show-grid">
                    <div class="grid col-md-1">
                        <label>G1</label>
                    </div>
                    <div class="grid col-md-1">
                        <input type="text" placeholder="" class="col-md-12 form-input" name="g1[date]" id="g1_date" value="<?php if(isset($decoded->g1->date)) { echo $decoded->g1->date; } ?>" />
                    </div>
                    <div class="grid col-md-2">
                        <input type="text" placeholder="" class="col-md-12 form-input" name="g1[type_delivery]" id="g1_type_delivery" value="<?php if(isset($decoded->g1->type_delivery)) { echo $decoded->g1->type_delivery; } ?>" />
                    </div>
                    <div class="grid col-md-1">
                        <input type="text" placeholder="" class="col-md-12 form-input" name="g1[aog]" id="g1_aog" value="<?php if(isset($decoded->g1->aog)) { echo $decoded->g1->aog; } ?>" />
                    </div>
                    <div class="grid col-md-1">
                        <input type="text" placeholder="" class="col-md-12 form-input" name="g1[sex]" id="g1_sex" value="<?php if(isset($decoded->g1->sex)) { echo $decoded->g1->sex; } ?>" />
                    </div>
                    <div class="grid col-md-2">
                        <input type="text" placeholder="" class="col-md-12 form-input" name="g1[birthplace]" id="g1_birthplace" value="<?php if(isset($decoded->g1->birthplace)) { echo $decoded->g1->birthplace; } ?>" />
                    </div>
                    <div class="grid col-md-2">
                        <input type="text" placeholder="" class="col-md-12 form-input" name="g1[birthweight]" id="g1_birthweight" value="<?php if(isset($decoded->g1->birthweight)) { echo $decoded->g1->birthweight; } ?>" />
                    </div>
                    <div class="grid col-md-2">
                        <input type="text" placeholder="" class="col-md-12 form-input" name="g1[remarks]" id="g1_remarks" value="<?php if(isset($decoded->g1->remarks)) { echo $decoded->g1->remarks; } ?>" />
                    </div>
                </div>
                <div class="row show-grid">
                    <div class="grid col-md-1">
                        <label>G2</label>
                    </div>
                    <div class="grid col-md-1">
                        <input type="text" placeholder="" class="col-md-12 form-input" name="g2[date]" id="g2_date" value="<?php if(isset($decoded->g2->date)) { echo $decoded->g2->date; } ?>" />
                    </div>
                    <div class="grid col-md-2">
                        <input type="text" placeholder="" class="col-md-12 form-input" name="g2[type_delivery]" id="g2_type_delivery" value="<?php if(isset($decoded->g2->type_delivery)) { echo $decoded->g2->type_delivery; } ?>" />
                    </div>
                    <div class="grid col-md-1">
                        <input type="text" placeholder="" class="col-md-12 form-input" name="g2[aog]" id="g2_aog" value="<?php if(isset($decoded->g2->aog)) { echo $decoded->g2->aog; } ?>" />
                    </div>
                    <div class="grid col-md-1">
                        <input type="text" placeholder="" class="col-md-12 form-input" name="g2[sex]" id="g2_sex" value="<?php if(isset($decoded->g2->sex)) { echo $decoded->g2->sex; } ?>" />
                    </div>
                    <div class="grid col-md-2">
                        <input type="text" placeholder="" class="col-md-12 form-input" name="g2[birthplace]" id="g2_birthplace" value="<?php if(isset($decoded->g2->birthplace)) { echo $decoded->g2->birthplace; } ?>" />
                    </div>
                    <div class="grid col-md-2">
                        <input type="text" placeholder="" class="col-md-12 form-input" name="g2[birthweight]" id="g2_birthweight" value="<?php if(isset($decoded->g2->birthweight)) { echo $decoded->g2->birthweight; } ?>" />
                    </div>
                    <div class="grid col-md-2">
                        <input type="text" placeholder="" class="col-md-12 form-input" name="g2[remarks]" id="g2_remarks" value="<?php if(isset($decoded->g2->remarks)) { echo $decoded->g2->remarks; } ?>" />
                    </div>
                </div>
                <div class="row show-grid">
                    <div class="grid col-md-1">
                        <label>G3</label>
                    </div>
                    <div class="grid col-md-1">
                        <input type="text" placeholder="" class="col-md-12 form-input" name="g3[date]" id="g3_date" value="<?php if(isset($decoded->g3->date)) { echo $decoded->g3->date; } ?>" />
                    </div>
                    <div class="grid col-md-2">
                        <input type="text" placeholder="" class="col-md-12 form-input" name="g3[type_delivery]" id="g3_type_delivery" value="<?php if(isset($decoded->g3->type_delivery)) { echo $decoded->g3->type_delivery; } ?>" />
                    </div>
                    <div class="grid col-md-1">
                        <input type="text" placeholder="" class="col-md-12 form-input" name="g3[aog]" id="g3_aog" value="<?php if(isset($decoded->g3->aog)) { echo $decoded->g3->aog; } ?>" />
                    </div>
                    <div class="grid col-md-1">
                        <input type="text" placeholder="" class="col-md-12 form-input" name="g3[sex]" id="g3_sex" value="<?php if(isset($decoded->g3->sex)) { echo $decoded->g3->sex; } ?>" />
                    </div>
                    <div class="grid col-md-2">
                        <input type="text" placeholder="" class="col-md-12 form-input" name="g3[birthplace]" id="g3_birthplace" value="<?php if(isset($decoded->g3->birthplace)) { echo $decoded->g3->birthplace; } ?>" />
                    </div>
                    <div class="grid col-md-2">
                        <input type="text" placeholder="" class="col-md-12 form-input" name="g3[birthweight]" id="g3_birthweight" value="<?php if(isset($decoded->g3->birthweight)) { echo $decoded->g3->birthweight; } ?>" />
                    </div>
                    <div class="grid col-md-2">
                        <input type="text" placeholder="" class="col-md-12 form-input" name="g3[remarks]" id="g3_remarks" value="<?php if(isset($decoded->g3->remarks)) { echo $decoded->g3->remarks; } ?>" />
                    </div>
                </div>
                <div class="row show-grid">
                    <div class="grid col-md-1">
                        <label>G4</label>
                    </div>
                    <div class="grid col-md-1">
                        <input type="text" placeholder="" class="col-md-12 form-input" name="g4[date]" id="g4_date" value="<?php if(isset($decoded->g4->date)) { echo $decoded->g4->date; } ?>" />
                    </div>
                    <div class="grid col-md-2">
                        <input type="text" placeholder="" class="col-md-12 form-input" name="g4[type_delivery]" id="g4_type_delivery" value="<?php if(isset($decoded->g4->type_delivery)) { echo $decoded->g4->type_delivery; } ?>" />
                    </div>
                    <div class="grid col-md-1">
                        <input type="text" placeholder="" class="col-md-12 form-input" name="g4[aog]" id="g4_aog" value="<?php if(isset($decoded->g4->aog)) { echo $decoded->g4->aog; } ?>" />
                    </div>
                    <div class="grid col-md-1">
                        <input type="text" placeholder="" class="col-md-12 form-input" name="g4[sex]" id="g4_sex" value="<?php if(isset($decoded->g4->sex)) { echo $decoded->g4->sex; } ?>" />
                    </div>
                    <div class="grid col-md-2">
                        <input type="text" placeholder="" class="col-md-12 form-input" name="g4[birthplace]" id="g4_birthplace" value="<?php if(isset($decoded->g4->birthplace)) { echo $decoded->g4->birthplace; } ?>" />
                    </div>
                    <div class="grid col-md-2">
                        <input type="text" placeholder="" class="col-md-12 form-input" name="g4[birthweight]" id="g4_birthweight" value="<?php if(isset($decoded->g4->birthweight)) { echo $decoded->g4->birthweight; } ?>" />
                    </div>
                    <div class="grid col-md-2">
                        <input type="text" placeholder="" class="col-md-12 form-input" name="g4[remarks]" id="g4_remarks" value="<?php if(isset($decoded->g4->remarks)) { echo $decoded->g4->remarks; } ?>" />
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row show-grid">
                    <div class="header grid col-md-12">Reproductive History</div>
                </div>
                <div class="row show-grid">
                    <div class="grid col-md-3 textleft">
                        <label>Age of First Coitus</label>
                    </div>
                    <div class="grid col-md-3">
                        <input type="text" placeholder="" class="col-md-12 form-input" name="rh[age_fcoitus]" id="rh_age_fcoitus" value="<?php if(isset($decoded->rh->age_fcoitus)) { echo $decoded->rh->age_fcoitus; } ?>" />
                    </div>
                    <div class="grid col-md-3 textleft">
                        <label>Frequency of Coitus</label>
                    </div>
                    <div class="grid col-md-3">
                        <input type="text" placeholder="" class="col-md-12 form-input" name="rh[frequency_coitus]" id="rh_frequency_coitus" value="<?php if(isset($decoded->rh->frequency_coitus)) { echo $decoded->rh->frequency_coitus; } ?>" />
                    </div>
                    <div class="grid col-md-3 textleft">
                        <label>History of STI</label>
                    </div>
                    <div class="grid col-md-3">
                        <input type="text" placeholder="" class="col-md-12 form-input" name="rh[sti]" id="rh_sti" value="<?php if(isset($decoded->rh->sti)) { echo $decoded->rh->sti; } ?>" />
                    </div>
                    <div class="grid col-md-3 textleft">
                        <label>No. of sexual partners</label>
                    </div>
                    <div class="grid col-md-3">
                        <input type="text" placeholder="No. of sexual partners and their occupation" class="col-md-12 form-input" name="rh[no_sexpartners]" id="rh_no_sexpartners" value="<?php if(isset($decoded->rh->no_sexpartners)) { echo $decoded->rh->no_sexpartners; } ?>" />
                    </div>
                    <div class="grid col-md-3 textleft">
                        <label>Unusual sexual practices</label>
                    </div>
                    <div class="grid col-md-3">
                        <input type="text" placeholder="" class="col-md-12 form-input" name="rh[unusual_sexpractice]" id="rh_unusual_sexpractice" value="<?php if(isset($decoded->rh->unusual_sexpractice)) { echo $decoded->rh->unusual_sexpractice; } ?>" />
                    </div>
                    <div class="grid col-md-3 textleft">
                        <label>Contraception </label>
                    </div>
                    <div class="grid col-md-3">
                        <input type="text" placeholder="Type and duration of use" class="col-md-12 form-input" name="rh[contraception]" id="rh_contraception" value="<?php if(isset($decoded->rh->contraception)) { echo $decoded->rh->contraception; } ?>" />
                    </div>
                    <div class="grid col-md-3 textleft">
                        <label>Symptoms </label>
                    </div>
                    <div class="grid col-md-3">
                        <input type="text" placeholder="Dyspareunia, post-coital bleeding, discharges   " class="col-md-12 form-input" name="rh[symptoms]" id="rh_symptoms" value="<?php if(isset($decoded->rh->symptoms)) { echo $decoded->rh->symptoms; } ?>" />
                    </div>
                    <div class="grid col-md-3 textleft">
                        <label>Others</label>
                    </div>
                    <div class="grid col-md-3">
                        <input type="text" placeholder="" class="col-md-12 form-input" name="rh[others]" id="rh_others" value="<?php if(isset($decoded->rh->others)) { echo $decoded->rh->others; } ?>" />
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <!-- <div class="col-md-12 textleft header2">Physical Examination</div> -->
                    <div class="header grid col-md-12">Physical Examination</div>
                </div>

                <div class="row show-grid">
                    <div class="grid col-md-12 blueback"><label>Anthropometrics</label></div>
                    <div class="grid col-md-4 textleft">
                        <label>Height</label>
                    </div>
                    <div class="grid col-md-4">
                        <input type="text" class="col-md-12 form-input" name="pe[height]" id="pe_height" placeholder="" value="<?php if(isset($decoded->pe->height)) { echo $decoded->pe->height; } ?>">
                    </div>
                    <div class="grid col-md-4 textleft">
                        <label> in cm. </label>
                    </div>
                    <div class="grid col-md-4 textleft">
                        <label>Weight</label>
                    </div>
                    <div class="grid col-md-4">
                        <input type="text" class="col-md-12 form-input" name="pe[weight]" id="pe_weight" placeholder="" value="<?php if(isset($decoded->pe->weight)) { echo $decoded->pe->weight; } ?>">
                    </div>
                    <div class="grid col-md-4 textleft">
                        <label> in kg. </label>
                    </div> 
                    <div class="grid col-md-4 textleft">
                        <label>BMI</label>
                    </div>
                    <div class="grid col-md-4">
                        <input type="text" class="col-md-12 form-input" name="pe[bmi]" id="pe_bmi" readonly placeholder="" value="<?php if(isset($decoded->pe->bmi)) { echo $decoded->pe->bmi; } ?>" >
                    </div>
                    <div class="grid col-md-4 textleft">
                        <label> in kg/m2 </label>
                    </div> 

                    <div class="grid col-md-12 blueback"><label>Vital Signs</label></div>
                    <div class="grid col-md-4 textleft">
                        <label>HR</label>
                    </div>
                    <div class="grid col-md-8">
                        <input type="text" class="col-md-12 form-input" name="pe[hr]" id="pe_hr" placeholder="" value="<?php if(isset($decoded->pe->hr)) { echo $decoded->pe->hr; } ?>">
                    </div>  
                    <div class="grid col-md-4 textleft">
                        <label>RR</label>
                    </div>
                    <div class="grid col-md-8">
                        <input type="text" class="col-md-12 form-input" name="pe[rr]" id="pe_rr" placeholder="" value="<?php if(isset($decoded->pe->rr)) { echo $decoded->pe->rr; } ?>">
                    </div>
                    <div class="grid col-md-4 textleft">
                        <label>T</label>
                    </div>
                    <div class="grid col-md-8">
                        <input type="text" class="col-md-12 form-input" name="pe[t]" id="pe_t" placeholder="" value="<?php if(isset($decoded->pe->t)) { echo $decoded->pe->t; } ?>">
                    </div>                   
                    <div class="grid col-md-4 textleft">
                        <label>Pain Scale (Click Image to Select)</label>
                    </div>
                    <div class="grid col-md-8">
                        <input type="hidden" name="pe[pain_scale_value]" id="pe_pain_scale_value" value="<?php if(isset($decoded->pe->pain_scale_value)) { echo $decoded->pe->pain_scale_value; } ?>" />
                        <div class="col-md-2">
                            <label> 0 </label>
                            <img src={{ asset("assets/images/nohurt.png") }} width="45" id="pe_pain_scale" value="0" class="painimg painimg0" />
                        </div>
                        <div class="col-md-2">
                            <label> 1 </label>
                            <img src={{ asset("assets/images/hurtlittle.png") }} width="45" id="pe_pain_scale" value="1" class="painimg painimg1" />
                        </div>
                        <div class="col-md-2">
                            <label> 2 </label>
                            <img src={{ asset("assets/images/hurtlittlemore.png") }} width="45" id="pe_pain_scale" value="2" class="painimg painimg2" />
                        </div>
                        <div class="col-md-2">
                            <label> 3 </label>
                            <img src={{ asset("assets/images/hurtwhilelot.png") }} width="45" id="pe_pain_scale" value="3" class="painimg painimg3" />
                        </div>
                        <div class="col-md-2">
                            <label> 4 </label>
                            <img src={{ asset("assets/images/hurtevenmore.png") }} width="45" id="pe_pain_scale" value="4" class="painimg painimg4" />
                        </div>
                        <div class="col-md-2">
                            <label> 5 </label>
                            <img src={{ asset("assets/images/hurtworst.png") }} width="45" id="pe_pain_scale" value="5" class="painimg painimg5" />   
                        </div>                 
                    </div>

                    <div class="grid col-md-12 blueback"><label>Observations</label></div> 
                    <div class="grid col-md-4 textleft">
                        <label>Head &amp; Neck </label>
                    </div>
                    <div class="grid col-md-8">
                        <input type="text" class="col-md-12 form-input" name="pe[head]" id="pe_head" placeholder="" value="<?php if(isset($decoded->pe->head)) { echo $decoded->pe->head; } ?>">
                    </div>
                    <div class="grid col-md-4 textleft">
                        <label>Eyes</label>
                    </div>
                    <div class="grid col-md-8">
                        <input type="text" class="col-md-12 form-input" name="pe[eyes]" id="pe_eyes" placeholder="" value="<?php if(isset($decoded->pe->eyes)) { echo $decoded->pe->eyes; } ?>">
                    </div>                    
                    <div class="grid col-md-4 textleft">
                        <label>Cardiovascular</label>
                    </div>
                    <div class="grid col-md-8">
                        <input type="text" class="col-md-12 form-input" name="pe[cardiovascular]" id="pe_cardiovascular" placeholder="" value="<?php if(isset($decoded->pe->cardiovascular)) { echo $decoded->pe->cardiovascular; } ?>">
                    </div>    
                    <div class="grid col-md-4 textleft">
                        <label>Breasts and Axillae </label>
                    </div>
                    <div class="grid col-md-8">
                        <input type="text" class="col-md-12 form-input" name="pe[breast_axillae]" id="pe_breast_axillae" placeholder="" value="<?php if(isset($decoded->pe->breast_axillae)) { echo $decoded->pe->breast_axillae; } ?>">
                    </div>    
                    <div class="grid col-md-4 textleft">
                        <label>Chest and Lungs </label>
                    </div>
                    <div class="grid col-md-8">
                        <input type="text" class="col-md-12 form-input" name="pe[chest_lungs]" id="pe_chest_lungs" placeholder="" value="<?php if(isset($decoded->pe->chest_lungs)) { echo $decoded->pe->chest_lungs; } ?>">
                    </div>    
                    <div class="grid col-md-4 textleft">
                        <label>Back and Spine </label>
                    </div>
                    <div class="grid col-md-8">
                        <input type="text" class="col-md-12 form-input" name="pe[back]" id="pe_back" placeholder="" value="<?php if(isset($decoded->pe->back)) { echo $decoded->pe->back; } ?>">
                    </div>    
                    <div class="grid col-md-4 textleft">
                        <label>Abdomen </label>
                    </div>
                    <div class="grid col-md-8">
                        <input type="text" class="col-md-12 form-input" name="pe[abdomen]" id="pe_abdomen" placeholder="" value="<?php if(isset($decoded->pe->abdomen)) { echo $decoded->pe->abdomen; } ?>">
                    </div>    
                    <div class="grid col-md-4 textleft">
                        <label>Pelvis/GU Tract </label>
                    </div>
                    <div class="grid col-md-8">
                        <input type="text" class="col-md-12 form-input" name="pe[pelvis]" id="pe_pelvis" placeholder="" value="<?php if(isset($decoded->pe->pelvis)) { echo $decoded->pe->pelvis; } ?>">
                    </div>    
                    <div class="grid col-md-4 textleft">
                        <label>Rectal </label>
                    </div>
                    <div class="grid col-md-8">
                        <input type="text" class="col-md-12 form-input" name="pe[rectal]" id="pe_rectal" placeholder="" value="<?php if(isset($decoded->pe->rectal)) { echo $decoded->pe->rectal; } ?>">
                    </div>    
                    <div class="grid col-md-4 textleft">
                        <label>Upper Extremities </label>
                    </div>
                    <div class="grid col-md-8">
                        <input type="text" class="col-md-12 form-input" name="pe[uextremities]" id="pe_uextremities" placeholder="" value="<?php if(isset($decoded->pe->uextremities)) { echo $decoded->pe->uextremities; } ?>">
                    </div>    
                    <div class="grid col-md-4 textleft">
                        <label>Lower Extremities </label>
                    </div>
                    <div class="grid col-md-8">
                        <input type="text" class="col-md-12 form-input" name="pe[lextremities]" id="pe_lextremities" placeholder="" value="<?php if(isset($decoded->pe->lextremities)) { echo $decoded->pe->lextremities; } ?>">
                    </div>    
                    <div class="grid col-md-4 textleft">
                        <label>Integumentary </label>
                    </div>
                    <div class="grid col-md-8">
                        <input type="text" class="col-md-12 form-input" name="pe[integumentary]" id="pe_integumentary" placeholder="" value="<?php if(isset($decoded->pe->integumentary)) { echo $decoded->pe->integumentary; } ?>">
                    </div>    
                    <div class="grid col-md-4 textleft">
                        <label>Skin </label>
                    </div>
                    <div class="grid col-md-8">
                        <input type="text" class="col-md-12 form-input" name="pe[skin]" id="pe_skin" placeholder="" value="<?php if(isset($decoded->pe->skin)) { echo $decoded->pe->skin; } ?>">
                    </div>    
                    <div class="grid col-md-4 textleft">
                        <label>Nails </label>
                    </div>
                    <div class="grid col-md-8">
                        <input type="text" class="col-md-12 form-input" name="pe[nails]" id="pe_nails" placeholder="" value="<?php if(isset($decoded->pe->nails)) { echo $decoded->pe->nails; } ?>">
                    </div>    
                    <div class="grid col-md-4 textleft">
                        <label>Hair &amp; Scalp </label>
                    </div>
                    <div class="grid col-md-8">
                        <input type="text" class="col-md-12 form-input" name="pe[hair_scalp]" id="pe_hair_scalp" placeholder="" value="<?php if(isset($decoded->pe->hair_scalp)) { echo $decoded->pe->hair_scalp; } ?>">
                    </div>   
                 </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <!-- <div class="col-md-12 textleft header2">Physical Examination</div> -->
                    <div class="header grid col-md-12">Impression and Diagnosis</div>
                </div>
                <div class="row show-grid">
                    <div class="grid col-md-4 textleft">
                        <label>Initial Impressions </label>
                    </div>
                    <div class="grid col-md-8">
                        <input type="text" class="col-md-12 form-input" name="pe[intial_impressions]" id="pe_initial_impression" placeholder="" value="<?php if(isset($decoded->pe->intial_impressions)) { echo $decoded->pe->intial_impressions; } ?>">
                    </div>
                    <div class="grid col-md-4 textleft">
                        <label>Proposed Diagnostic and Treatment Plan </label>
                    </div>
                    <div class="grid col-md-8">
                        <input type="text" class="col-md-12 form-input" name="pe[diagnostic_treatment]" id="pe_diagnostic_treatment" placeholder="" value="<?php if(isset($decoded->pe->diagnostic_treatment)) { echo $decoded->pe->diagnostic_treatment; } ?>">
                    </div>
                </div>   
            </div>


            <br clear="all" />

            <hr />

            @if(isset($intent))
                <div class="row">
                    <div class=" col-md-12">
                        <!-- <input type="Submit"> -->
                        <button id = "formSubmit" type="button" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            @else
                <a href="{{ url('admin') }}"> Back </a>
            @endif
            {!! Form::close() !!}
        </div>

        <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
        <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <script src="../../resources/assets/plugins/bootstrap-datepicker.js"></script>
        <script src="../../resources/assets/plugins/bootstrap-datetimepicker.min.js"></script>
        <script>
                $('.datepicker').datepicker();
                $(".datetimepicker").datetimepicker({format: 'mm/dd/yyyy hh:ii', forceParse: true});
        </script>
        <!-- Timer -->
         <script src={{ asset('\assets\js\adultform.js') }}></script>
    </body>
</html>
