<!DOCTYPE html>
<html>
    <head>
        <title>ADULT ENCOUNTER FORM</title>

        <link href="//fonts.googleapis.com/css?family=Roboto:100" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href={{ asset('assets\css\bootstrap.min.css') }}>
        <link rel="stylesheet" href={{ asset('assets\css\bootstrap-theme.min.css') }}>
        <meta name="csrf-token" content="<?php echo csrf_token() ?>"/>


        <style>
            html, body {
                height: 100%;
            }
            body {
                color:#111;
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 500;
                font-family: 'Roboto';
            }
            .container {
                text-align: center;
                margin-bottom:25px;
            }
            .content {

            }
            .title {
                font-size: 42px;
            }
            .subtitle {
                font-size: 20px;
                font-weight:500;
                margin-bottom: 25px;
            }
            .show-grid div[class^=col-] {
                padding: 0px;
                background-color: #eee;
                background-color: rgba(86,61,124,.15);
                border: 1px solid #ddd;
                border: 1px solid rgba(86,61,124,.2);
            }
            div.header {
                color: #fff;
                font-weight: 700;
                padding:10px;
                background-color: blue;
                text-transform: uppercase;
            }
            div.header2 {
                color: #000;
                font-weight: 700;
                padding:10px;
                text-transform: uppercase;
            }
            div.double-height {
                height: 82px !important;
            }
            label.double-height {
                height: 80px !important;
            }
            .whiteback {
                background-color: #fff !important;
            }
            input[type=text], textarea, label {
                border: none;
                width: 100%;
                height: 40px;
                padding: 10px;
            }
            input[type=text].col-md-4 {
                width: 33.33333333%;
            }
            label, input {
                height: auto;
            }
            textarea {
                height:100%;
                margin:0;
            }
            .textareaheight {
                margin-bottom: -5px;
            }
            label {
                margin-bottom: 0 !important;
            }
            .textleft {
                text-align: left;
            }
            .checkbox {
                padding:0 10px !important;
                margin-bottom:0 !important;
            }
            input.inlinefield {
                float: right;
                width: 70%;
                border-bottom: 1px solid #BBB;
                margin-top: -7px;
                padding:0 10px 4px !important;
                height:24px;
            }
            .checkbox label {
                padding:0 0 12px 30px !important;
            }
        </style>
        
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="title">ASMPH</div>
                <div class="subtitle">Adult Clinical Encounter</div>
            </div>
            {!! Form::open(array( 'method' => 'POST', 'url'=>'save', 'id'=>'ASMPHForm', 'name'=>'ASMPHForm', 'class'=>'form-horizontal' )) !!}
                <input type="hidden" name="time" id="time" value="">
                <div class="form-group">
                    <div class="row show-grid">
                        <div class="header col-md-12">General Information</div>
                    </div>
                    <div class="row show-grid">
                        <div class="col-md-6"><input type="text" placeholder="Name" class="form-input" name="name" id="name" /></div>
                        <div class="col-md-6"><input type="text" placeholder="Student-in-charge" class="form-input" name="student_in_charge" id="student_in_charge" /></div>
                    </div>
                    <div class="row show-grid">
                    <div class="col-md-6"><input type="text" placeholder="Date of Birth (MM/DD/YYYY)" class="form-input" name="date_of_birth" id="date_of_birth" /></div>
                    <div class="col-md-6"><input type="text" placeholder="Date of Admission" class="form-input" name="date_of_admission" id="date_of_admission" /></div>
                    </div>
                    <div class="row show-grid">
                        <div class="col-md-6"><input type="text" placeholder="Age" class="col-md-4 form-input" name="age" id="age"/><input type="text" placeholder="Sex" class="col-md-4 form-input" name="sex" id="sex"/><input type="text" placeholder="Civil Status" class="col-md-4 form-input" name="civil_status" id="civil_status"/></div>
                        <div class="col-md-6"><input type="text" placeholder="Date of Interview" class="form-input" name="date_of_interview" id="date_of_interview"/></div>
                    </div>
                    <div class="row show-grid">
                    <div class="col-md-6"><input type="text" placeholder="Religion" class="form-input" name="religion" id="religion" /></div>
                    <div class="col-md-6"><input type="text" placeholder="Place of Interview" class="form-input" name="place_of_interview" id="place_of_interview" /></div>
                    </div>
                    <div class="row show-grid">
                        <div class="col-md-6 double-height"><textarea type="text" placeholder="Address" class="form-input" name="address" id="address"></textarea></div>
                        <div class="col-md-6"><input type="text" placeholder="Informant" class="form-input" name="informant" id="informant"/><input type="text" placeholder="Relation" class="form-input" name="relation" id="relation" /></div>
                    </div>
                    <div class="row show-grid">
                        <div class="col-md-6"><input type="text" placeholder="Attending Physician" class="form-input" name="attending_physician" id="attending_physician" /></div>
                        <div class="col-md-6 whiteback" >
                            <label class="col-md-1">Reliability</label>
                            <div class="radio-inline">
                                <label>
                                  <input type="radio" name="reliability" id="reliability" value="Excellent"> 
                                  Excellent
                                </label>
                            </div>
                            <div class="radio-inline">
                                <label>
                                  <input type="radio" name="reliability" id="reliability" value="Very Good"> 
                                  Very Good
                                </label>
                            </div>
                            <div class="radio-inline">
                                <label>
                                  <input type="radio" name="reliability" id="reliability" value="Fair"> 
                                  Fair
                                </label>
                            </div>
                            <div class="radio-inline" >
                                <label>
                                  <input type="radio" name="reliability" id="reliability" value="Poor"> 
                                  Poor
                                </label>
                            </div>
                            <br clear="all" />
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row show-grid">
                        <div class="header col-md-12">Clinical History</div>
                    </div>
                    <div class="row show-grid">
                        <div class="col-md-12"><input type="text" placeholder="Chief Complaint" class="form-input" name="" /></div>
                    </div>
                    <div class="row show-grid">
                        <div class="col-md-12 textareaheight"><textarea type="text" placeholder="Address" class="form-input" name="" rows="18" ></textarea></div>
                    </div>
                    <div class="row show-grid">
                        <div class="col-md-8 textareaheight"><textarea type="text" placeholder="Temporal Profile" class="form-input" name="" rows="8" ></textarea></div>
                        <div class="col-md-4 textareaheight"><textarea type="text" placeholder="Legend" class="form-input" name="" rows="8" ></textarea></div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row show-grid">
                        <div class="header col-md-12">Review of Systems</div>
                    </div>
                    <div class="row show-grid">
                        <div class="col-md-3"><label class="whiteback">General</label></div>
                        <div class="col-md-9 whiteback textleft">
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Fever
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Weight Gain
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Weight Loss
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Weakness
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Fatigue
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Others
                                </label>
                            </div>
                            <br clear="all" />
                        </div>

                        <div class="col-md-3 double-height">
                            <label class="whiteback double-height">Musculoskeletal/ Integumentary</label>
                        </div>
                        <div class="col-md-9 whiteback textleft">
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Ashes
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Lumps
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Sores
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Itching
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Muscle Pains
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Join Pains
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Changes in color
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Joint Swelling
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Chnages in Hair and Nails
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Others
                                </label>
                            </div>
                            <br clear="all" />
                        </div>

                        <div class="col-md-3 double-height">
                            <label class="whiteback double-height">HEENT</label>
                        </div>
                        <div class="col-md-9 whiteback textleft">
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Headache
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Dizziness
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Blurring of vision
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Tinnitus
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Deafness
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Expistaxis
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Frequent Colds
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Hoarseness
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Dry Mouth
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Gum Bleeding
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Gum Bleeding
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Others
                                </label>
                            </div>
                            <br clear="all" />
                        </div>

                        <div class="col-md-3"><label class="whiteback">Respiratory</label></div>
                        <div class="col-md-9 whiteback textleft">
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Dyspnea
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Hemoptysis
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Cough
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Weezhing
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Others
                                </label>
                            </div>
                            <br clear="all" />
                        </div>

                        <div class="col-md-3">
                            <label class="whiteback">Cardiovascular</label>
                        </div>
                        <div class="col-md-9 whiteback textleft">
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Palpitations
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Chest Pains
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Syncope
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Orthopnea
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Others
                                </label>
                            </div>
                            <br clear="all" />
                        </div>

                        <div class="col-md-3 double-height">
                            <label class="whiteback double-height">Gastrointestinal</label>
                        </div>
                        <div class="col-md-9 whiteback textleft">
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Nausea
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Vomiting
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Dysphagia
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Heartburn
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Constipation
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Diarrhea
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Rectal Bleeding
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Jaundice
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Others
                                </label>
                            </div>
                            <br clear="all" />
                        </div>

                        <div class="col-md-3"><label class="whiteback">Endocrine</label></div>
                        <div class="col-md-9 whiteback textleft">
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Excessive Sweating
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Heat Intolerance
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Polyuria
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Excessive Thirst
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Cold Intolerance
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Others
                                </label>
                            </div>
                            <br clear="all" />
                        </div>

                        <div class="col-md-3"><label class="whiteback">Genitourinary</label></div>
                        <div class="col-md-9 whiteback textleft">
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Dysuria
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Sexual Dysfunction
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Discharge
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Others
                                </label>
                            </div>
                            <br clear="all" />
                        </div>

                        <div class="col-md-3"><label class="whiteback">Neurological</label></div>
                        <div class="col-md-9 whiteback textleft">
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Seizures
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Tremors
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Others
                                </label>
                            </div>
                            <br clear="all" />
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row show-grid">
                        <div class="header col-md-12">Past Medical History</div>
                    </div>
                    <div class="row show-grid">
                        <div class="col-md-4 whiteback textleft">
                            <div class="checkbox">
                                <label>
                                  <input type="checkbox"> Thyroid Disease
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                  <input type="checkbox"> Tuberculosis
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                  <input type="checkbox"> Asthma
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                  <input type="checkbox"> Pneumonia
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                  <input type="checkbox"> Gastroenteritis
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                  <input type="checkbox"> Diabetes
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                  <input type="checkbox"> Hypertension
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                  <input type="checkbox"> Stroke
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                  <input type="checkbox"> Psychiatric Condition
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                  <input type="checkbox"> Kidney disease
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                  <input type="checkbox"> Liver disease
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                  <input type="checkbox"> Allergies <input type="text" class="inlinefield" />
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                  <input type="checkbox"> Cancer Site <input type="text" class="inlinefield" />
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                  <input type="checkbox"> Musculo Skeletal Disease
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                  <input type="checkbox"> Others
                                </label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="col-md-12"><label>Prior Hospitalization</label></div>

                            <div class="col-md-6"><input type="text" placeholder="Date" class="form-input" name="" /></div>
                            <div class="col-md-6"><input type="text" placeholder="Date" class="form-input" name="" /></div>
                            <div class="col-md-6"><input type="text" placeholder="" class="form-input" name="" /></div>
                            <div class="col-md-6"><input type="text" placeholder="" class="form-input" name="" /></div>
                            <div class="col-md-6"><input type="text" placeholder="" class="form-input" name="" /></div>
                            <div class="col-md-6"><input type="text" placeholder="" class="form-input" name="" /></div>
                            <div class="col-md-6"><input type="text" placeholder="" class="form-input" name="" /></div>
                            <div class="col-md-6"><input type="text" placeholder="" class="form-input" name="" /></div>
                            <div class="col-md-12"><label>Prior Surgery</label></div>

                            <div class="col-md-6"><input type="text" placeholder="Date" class="form-input" name="" /></div>
                            <div class="col-md-6"><input type="text" placeholder="Date" class="form-input" name="" /></div>
                            <div class="col-md-6"><input type="text" placeholder="" class="form-input" name="" /></div>
                            <div class="col-md-6"><input type="text" placeholder="" class="form-input" name="" /></div>
                            <div class="col-md-6"><input type="text" placeholder="" class="form-input" name="" /></div>
                            <div class="col-md-6"><input type="text" placeholder="" class="form-input" name="" /></div>
                            <div class="col-md-6"><input type="text" placeholder="" class="form-input" name="" /></div>
                            <div class="col-md-6"><input type="text" placeholder="" class="form-input" name="" /></div>
                        </div>
                    </div>
                    <div class="row show-grid">
                        <div class="col-md-12 textleft">
                            <label>REVIEW OF CURRENT MEDICATIONS ( Includes prescribed, herbal, and over the counter medications)</label>
                            <textarea type="text" class="form-input" name="" rows="10" ></textarea></div>
                    </div>
                    <div class="row show-grid">
                        <div class="col-md-12 textleft">
                            <label>PRESENT LABS / ANCILLARY TESTS</label>
                            <textarea type="text" class="form-input" name="" rows="10" ></textarea></div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row show-grid">
                        <div class="header col-md-12">Family History</div>
                    </div>
                    <div class="row show-grid">

                        <div class="col-md-6">
                            <label>GENOGRAM:</label>
                            <textarea type="text" class="form-input" name="" rows="10" ></textarea>
                        </div>

                        <div class="col-md-6">
                            <label>LEGEND:</label>
                            <textarea type="text" class="form-input" name="" rows="10" ></textarea>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row show-grid">
                        <div class="header col-md-12">Immunization</div>
                    </div>
                    <div class="row show-grid">
                        <div class="col-md-2">
                            <label>Vaccine</label>
                        </div>
                        <div class="col-md-2">
                            <label># of doses received</label>
                        </div>
                        <div class="col-md-2">
                            <label>Year given</label>
                        </div>
                        <div class="col-md-2">
                            <label>Vaccine</label>
                        </div>
                        <div class="col-md-2">
                            <label># of doses received</label>
                        </div>
                        <div class="col-md-2">
                            <label>Year given</label>
                        </div>
                    </div>
                    <div class="row show-grid">
                        <div class="col-md-2">
                            <label>Influenza</label>
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="Doses" class="form-input" name="" />
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="Year" class="form-input" name="" />
                        </div>
                        <div class="col-md-2">
                            <label>Hepatitis B</label>
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="Doses" class="form-input" name="" />
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="Year" class="form-input" name="" />
                        </div>
                    </div>
                    <div class="row show-grid">
                        <div class="col-md-2">
                            <label>Pneumonia</label>
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="Doses" class="form-input" name="" />
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="Year" class="form-input" name="" />
                        </div>
                        <div class="col-md-2">
                            <label>MMR</label>
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="Doses" class="form-input" name="" />
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="Year" class="form-input" name="" />
                        </div>
                    </div>
                    <div class="row show-grid">
                        <div class="col-md-2">
                            <label>Tetanus</label>
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="Doses" class="form-input" name="" />
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="Year" class="form-input" name="" />
                        </div>
                        <div class="col-md-2">
                            <label>Chicken Pox</label>
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="Doses" class="form-input" name="" />
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="Year" class="form-input" name="" />
                        </div>
                    </div>
                    <div class="row show-grid">
                        <div class="col-md-2">
                            <label>OTHERS</label>
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="Doses" class="form-input" name="" />
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="Year" class="form-input" name="" />
                        </div>
                        <div class="col-md-2">
                            <label>OTHERS</label>
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="Doses" class="form-input" name="" />
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="Year" class="form-input" name="" />
                        </div>
                    </div>
                    <div class="row show-grid">
                        <div class="col-md-6 whiteback">
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Vaccinated by Medical Doctor
                                </label>
                            </div>
                        </div>

                        <div class="col-md-6 whiteback">
                            <div class="checkbox-inline">
                                <label>
                                  <input type="checkbox"> Vaccinated at Health Center
                                </label>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="form-group">
                    <div class="row show-grid">
                        <div class="header col-md-12">Personal History</div>
                    </div>
                    <div class="row show-grid">
                        <div class="col-md-3 textleft">
                            <label>Education</label>
                        </div>
                        <div class="col-md-9">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-3 textleft">
                            <label>Smoking</label>
                        </div>
                        <div class="col-md-9">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-3 textleft">
                            <label>Alcohol Intake</label>
                        </div>
                        <div class="col-md-9">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-3 textleft">
                            <label>Substance Use</label>
                        </div>
                        <div class="col-md-9">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-3 textleft">
                            <label>Physical Activity/Exercise</label>
                        </div>
                        <div class="col-md-9">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row show-grid">
                        <div class="header col-md-12">Social and Environtal History</div>
                    </div>
                    <div class="row show-grid">
                        <div class="col-md-3 textleft">
                            <label>Occupation</label>
                        </div>
                        <div class="col-md-3">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-3 textleft">
                            <label>Type of Dwelling</label>
                        </div>
                        <div class="col-md-3">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-3 textleft">
                            <label>Number of Household Members</label>
                        </div>
                        <div class="col-md-3">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-3 textleft">
                            <label>Drinking Water Source</label>
                        </div>
                        <div class="col-md-3">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-3 textleft">
                            <label>Electricity</label>
                        </div>
                        <div class="col-md-3">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-3 textleft">
                            <label>Waste and Garbage Disposal</label>
                        </div>
                        <div class="col-md-3">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-3 textleft">
                            <label>Exposure to Tobacco/Toxins/Biohazzards</label>
                        </div>
                        <div class="col-md-3">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-3 textleft">
                            <label>Pertinent History of Travel</label>
                        </div>
                        <div class="col-md-3">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>

                        <div class="col-md-3 textleft">
                            <label>Others</label>
                        </div>
                        <div class="col-md-3">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-3 textleft">
                            <label>&nbsp;</label>
                        </div>
                        <div class="col-md-3">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row show-grid">
                        <div class="header col-md-12">Reproductive History<br />(Male & Female: if applicable)</div>
                    </div>
                    <div class="row show-grid">
                        <div class="col-md-3 textleft">
                            <label>LMP</label>
                        </div>
                        <div class="col-md-3">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-3 textleft">
                            <label>Age of First Coitus</label>
                        </div>
                        <div class="col-md-3">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-3 textleft">
                            <label>PMP</label>
                        </div>
                        <div class="col-md-3">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-3 textleft">
                            <label>Number of Sexual Partners</label>
                        </div>
                        <div class="col-md-3">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-3 textleft">
                            <label>Menarche</label>
                        </div>
                        <div class="col-md-3">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-3 textleft">
                            <label>Contraceptive / HRT</label>
                        </div>
                        <div class="col-md-3">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-3 textleft">
                            <label>Interval</label>
                        </div>
                        <div class="col-md-3">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-3 textleft">
                            <label>Sexual Diseases / Disorders</label>
                        </div>
                        <div class="col-md-3">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>

                        <div class="col-md-3 textleft">
                            <label>Duration / Amount</label>
                        </div>
                        <div class="col-md-3">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-3 textleft">
                            <label>Others</label>
                        </div>
                        <div class="col-md-3">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row show-grid">
                        <div class="header col-md-12">Obstetric History</div>
                    </div>
                    <div class="row show-grid">
                        <div class="col-md-1 textleft double-height">
                            <label>Gravida</label>
                        </div>
                        <div class="col-md-1 textleft double-height">
                            <label>Date</label>
                        </div>
                        <div class="col-md-2 textleft double-height">
                            <label>Type of Delivery</label>
                        </div>
                        <div class="col-md-1 textleft double-height">
                            <label>AOG</label>
                        </div>
                        <div class="col-md-2 textleft double-height">
                            <label>Place of Delivery<br>Attended by</label>
                        </div>
                        <div class="col-md-1 textleft double-height">
                            <label>Sex</label>
                        </div>
                        <div class="col-md-2 textleft double-height">
                            <label>Birthweight<br>(Grams)</label>
                        </div>
                        <div class="col-md-2 textleft double-height">
                            <label>Remarks / Complications</label>
                        </div>
                    </div>
                    <div class="row show-grid">
                        <div class="col-md-1">
                            <label>G1</label>
                        </div>
                        <div class="col-md-1">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-1">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-1">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                    </div>
                    <div class="row show-grid">
                        <div class="col-md-1">
                            <label>G2</label>
                        </div>
                        <div class="col-md-1">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-1">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-1">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                    </div>
                    <div class="row show-grid">
                        <div class="col-md-1">
                            <label>G3</label>
                        </div>
                        <div class="col-md-1">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-1">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-1">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                    </div>
                    <div class="row show-grid">
                        <div class="col-md-1">
                            <label>G4</label>
                        </div>
                        <div class="col-md-1">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-1">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-1">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row show-grid">
                        <div class="header col-md-12">Stakeholder Analysis</div>
                    </div>
                    <div class="row show-grid">
                        <div class="col-md-2">
                            <label>Name/Role</label>
                        </div>
                        <div class="col-md-2">
                            <label>Stake/WIIFM</label>
                        </div>
                        <div class="col-md-2">
                            <label>Stand on the Issue</label>
                        </div>
                        <div class="col-md-2">
                            <label>Intensity of Stand</label>
                        </div>
                        <div class="col-md-2">
                            <label>Degree of Influence</label>
                        </div>
                        <div class="col-md-2">
                            <label>Insight/Action</label>
                        </div>
                    </div>

                    <div class="row show-grid">
                        <div class="col-md-2">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                    </div>

                    <div class="row show-grid">
                        <div class="col-md-2">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                    </div>

                    <div class="row show-grid">
                        <div class="col-md-2">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                    </div>

                    <div class="row show-grid">
                        <div class="col-md-2">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                    </div>

                    <div class="row show-grid">
                        <div class="col-md-2">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="" class="form-input" name="" />
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12 textleft header2">General Survey</div>
                    </div>

                    <div class="col-md-2">
                        <label>Anthropometrics :</label>
                    </div>
                    <div class="col-md-5">
                        <label>Height: <input type="text" placeholder="" class="form-input inlinefield" name="" /></label>
                    </div>
                    <div class="col-md-5">
                        <label>HC : <input type="text" placeholder="" class="form-input inlinefield" name="" /></label>
                    </div>
                    <div class="col-md-2">
                        <label></label>
                    </div>
                    <div class="col-md-5">
                        <label>Weight: <input type="text" placeholder="" class="form-input inlinefield" name="" /></label>
                    </div>
                    <div class="col-md-5">
                        <label>CC : <input type="text" placeholder="" class="form-input inlinefield" name="" /></label>
                    </div>
                    <div class="col-md-2">
                        <label></label>
                    </div>
                    <div class="col-md-5">
                        <label>BMI: <input type="text" placeholder="" class="form-input inlinefield" name="" /></label>
                    </div>
                    <div class="col-md-5">
                        <label>AC : <input type="text" placeholder="" class="form-input inlinefield" name="" /></label>
                    </div>

                    <div class="col-md-2">
                        <label>Vital Signs :</label>
                    </div>
                    <div class="col-md-5">
                        <label>HR: <input type="text" placeholder="" class="form-input inlinefield" name="" /></label>
                    </div>
                    <div class="col-md-5 textleft">
                        <label>Pain Scale :</label>
                    </div>

                    <div class="col-md-2">
                        <label>&nbsp;</label>
                    </div>
                    <div class="col-md-5">
                        <label>RR: <input type="text" placeholder="" class="form-input inlinefield" name="" /></label>
                    </div>
                    <div class="col-md-5"><label>Image of smileys go here.</label></div>

                    <div class="col-md-2">
                        <label>&nbsp;</label>
                    </div>
                    <div class="col-md-5">
                        <label>T: <input type="text" placeholder="" class="form-input inlinefield" name="" /></label>
                    </div>
                    <div class="col-md-5"><label>&nbsp;</label></div>

                    <div class="col-md-2">
                        <label>&nbsp;</label>
                    </div>
                    <div class="col-md-5">
                        <label>BP: <input type="text" placeholder="" class="form-input inlinefield" name="" /></label>
                    </div>
                    <div class="col-md-5"><label>&nbsp;</label></div>


                    <div class="col-md-5 textleft">
                        <label>Head &amp; Neck: <input type="text" placeholder="" class="form-input inlinefield" name="" /></label>
                        <label>Eyes: <input type="text" placeholder="" class="form-input inlinefield" name="" /></label>
                        <label>Cardiovascular: <input type="text" placeholder="" class="form-input inlinefield" name="" /></label>
                        <label>Breasts and Axillae: <input type="text" placeholder="" class="form-input inlinefield" name="" /></label>
                        <label>Chest and Lungs: <input type="text" placeholder="" class="form-input inlinefield" name="" /></label>
                        <label>Back and Spine: <input type="text" placeholder="" class="form-input inlinefield" name="" /></label>
                        <label>Abdomen: <input type="text" placeholder="" class="form-input inlinefield" name="" /></label>
                        <label>Pelvis/ GU Tract: <input type="text" placeholder="" class="form-input inlinefield" name="" /></label>
                        <label>Rectal: <input type="text" placeholder="" class="form-input inlinefield" name="" /></label>
                        <label>Upper Extremities: <input type="text" placeholder="" class="form-input inlinefield" name="" /></label>
                        <label>Lower Extremities: <input type="text" placeholder="" class="form-input inlinefield" name="" /></label>
                        <label>Integumentary: <input type="text" placeholder="" class="form-input inlinefield" name="" /></label>
                        <label>Skin: <input type="text" placeholder="" class="form-input inlinefield" name="" /></label>
                        <label>Nails: <input type="text" placeholder="" class="form-input inlinefield" name="" /></label>
                        <label>Hair &amp; Scalp: <input type="text" placeholder="" class="form-input inlinefield" name="" /></label>
                    </div>
                    <div class="col-md-7">
                        <label>Human figure go here.</label>
                    </div>
                </div>

                <br clear="all" />

                <hr />

                <div class="row">
                    <div class=" col-md-12">
                        <!-- <input type="Submit"> -->
                        <button id = "formSubmit" type = "button" class="btn btn-primary">SAVE</button>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>

        <!-- jQuery -->
        <script src={{ asset('assets\js\jquery-1.10.2.js') }}></script>
        <!-- Bootstrap Core JavaScript -->
        <script src={{ asset('assets\js\bootstrap.min.js') }}></script>
        <!-- Timer -->
         <script src={{ asset('\assets\js\adultform.js') }}></script>

    </body>
</html>
