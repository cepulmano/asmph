                <div class="row">
                    <div class="col-md-2">
                        <label class="col-md-12">Anthropometrics:</label>
                    </div>
                    <div class="col-md-5">
                        <label class="col-md-12">Height: 
                            <input type="text" placeholder="e.g. 5'11''" class=" form-input inlinefield" name="pe[height]" id="pe_height" /> <span class="rightlabel">ft </span>
                        </label>
                    </div>
                    <div class="col-md-5">
                        <label class="col-md-12"> </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label class="col-md-12"> </label>
                    </div>
                    <div class="col-md-5">
                        <label class="col-md-12">Weight: <input type="text" placeholder="" class="form-input inlinefield" name="pe[weight]" id="pe_weight" /> <span class="rightlabel">kg./lbs.</span></label>
                    </div>
                    <div class="col-md-5">
                        <label class="col-md-12"> </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label class="col-md-12"></label>
                    </div>
                    <div class="col-md-5">
                        <label class="col-md-12">BMI: <input type="text" placeholder="" class="form-input inlinefield" name="pe[bmi]" id="pe_bmi" /> <span class="rightlabel">kg/m2</span></label>
                    </div>
                    <div class="col-md-5">
                        <label class="col-md-12"> </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label class="col-md-12">Vital Signs:</label>
                    </div>
                    <div class="col-md-5">
                        <label class="col-md-12">HR: <input type="text" placeholder="" class="form-input inlinefield" name="pe[vitals]" id="pe_vitals" /> <span class="rightlabel"> </span></label>
                    </div>
                    <div class="col-md-5"><label class="col-md-12">&nbsp;</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label class="col-md-12">&nbsp;</label>
                    </div>
                    <div class="col-md-5">
                        <label class="col-md-12">RR: <input type="text" placeholder="" class="form-input inlinefield" name="pe[rr]" id="pe_rr" /> <span class="rightlabel"> </span></label>
                    </div>
                    <div class="col-md-5"><label class="col-md-12">&nbsp;</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label class="col-md-12">&nbsp;</label>
                    </div>
                    <div class="col-md-5">
                        <label class="col-md-12">T: <input type="text" placeholder="" class="form-input inlinefield" name="pe[t]" id="pe_t" /> <span class="rightlabel"> </span></label>
                    </div>
                    <div class="col-md-5 textleft">
                        <label class="col-md-12 textleft">Pain Scale :</label>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2">
                        <label class="col-md-12">&nbsp;</label>
                    </div>
                    <div class="col-md-5">
                        <label class="col-md-12">BP: <input type="text" placeholder="" class="form-input inlinefield" name="pe[bp]" id="pe_bp" /> <span class="rightlabel"> </span></label>
                    </div>
                    <div class="col-md-5">
                        <input type="hidden" name="pe[pain_scale_value]" id="pe_pain_scale_value" value="" />
                        <label class="col-md-12 textleft">
                            <img src={{ asset("assets/images/nohurt.png") }} width="50" id="pe_pain_scale" value="0" />
                            <img src={{ asset("assets/images/hurtlittle.png") }} width="50" id="pe_pain_scale" value="1" />
                            <img src={{ asset("assets/images/hurtlittlemore.png") }} width="50" id="pe_pain_scale" value="2" />
                            <img src={{ asset("assets/images/hurtwhilelot.png") }} width="50" id="pe_pain_scale" value="3" />
                            <img src={{ asset("assets/images/hurtevenmore.png") }} width="50" id="pe_pain_scale" value="4" />
                            <img src={{ asset("assets/images/hurtworst.png") }} width="50" id="pe_pain_scale" value="5" />
                        </label>
                    </div>                
                </div>
                <div class="col-md-12" style="height:50px">&nbsp;</div>

                <div class="col-md-6 textleft">
                    <label class="col-md-12">Head &amp; Neck: <input type="text" placeholder="" class="form-input inlinefield" name="pe[head]" id="pe_head" /></label>
                    <label class="col-md-12">Eyes: <input type="text" placeholder="" class="form-input inlinefield" name="pe[eyes]" id="pe_eyes" /></label>
                    <label class="col-md-12">Cardiovascular: <input type="text" placeholder="" class="form-input inlinefield" name="pe[cardiovascular]" id="pe_cardiovascular" /></label>
                    <label class="col-md-12">Breasts and Axillae: <input type="text" placeholder="" class="form-input inlinefield" name="pe[breast_axillae]" id="pe_breast_axillae" /></label>
                    <label class="col-md-12">Chest and Lungs: <input type="text" placeholder="" class="form-input inlinefield" name="pe[chest]" id="pe_chest" /></label>
                    <label class="col-md-12">Back and Spine: <input type="text" placeholder="" class="form-input inlinefield" name="pe[back]" id="pe_back" /></label>
                    <label class="col-md-12">Abdomen: <input type="text" placeholder="" class="form-input inlinefield" name="pe[abdomen]" id="pe_abdomen" /></label>
                    <label class="col-md-12">Pelvis/ GU Tract: <input type="text" placeholder="" class="form-input inlinefield" name="pe[pelvis]" id="pe_pelvis" /></label>
                    <label class="col-md-12">Rectal: <input type="text" placeholder="" class="form-input inlinefield" name="pe[rectal]" id="pe_rectal" /></label>
                    <label class="col-md-12">Upper Extremities: <input type="text" placeholder="" class="form-input inlinefield" name="pe[uextremitis]" id="pe_uextremitites" /></label>
                    <label class="col-md-12">Lower Extremities: <input type="text" placeholder="" class="form-input inlinefield" name="pe[lextremitites]" id="pe_lextremities" /></label>
                    <label class="col-md-12">Integumentary: <input type="text" placeholder="" class="form-input inlinefield" name="pe[integumentary]" id="pe_integumentary" /></label>
                    <label class="col-md-12">Skin: <input type="text" placeholder="" class="form-input inlinefield" name="pe[skin]" id="pe_skin" /></label>
                    <label class="col-md-12">Nails: <input type="text" placeholder="" class="form-input inlinefield" name="pe[nails]" id="pe_nails" /></label>
                    <label class="col-md-12">Hair &amp; Scalp: <input type="text" placeholder="" class="form-input inlinefield" name="pe[hair]" id="pe_hair" /></label>
                </div>
                <div class="col-md-6">
                    <label class="col-md-12 textleft">Human figure go here.</label>
                </div>