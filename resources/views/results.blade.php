<?php
?>

<!DOCTYPE html>
<html>
    <head>
        <title>ADULT ENCOUNTER FORM</title>

        <link href="//fonts.googleapis.com/css?family=Roboto:100" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href={{ asset('assets\css\bootstrap.min.css') }}>
        <link rel="stylesheet" href={{ asset('assets\css\bootstrap-theme.min.css') }}>
        <link rel="stylesheet" href={{ asset('assets\css\dataTables.bootstrap.min.css') }}>
        <link rel="stylesheet" href={{ asset('assets\css\responsive.bootstrap.min.css') }}>
        <meta name="csrf-token" content="<?php echo csrf_token() ?>"/>


        <style>
            html, body {
                height: 100%;
            }
            body {
                color:#111;
                margin: 0;
                padding: 0;
                width: 100%;
                font-weight: 700;
                font-family: 'Roboto';
            }

            .titleheader {
                text-align: center;
            }

            .title {
                font-size: 42px;
            }
            .subtitle {
                font-size: 20px;
                font-weight:500;
                margin-bottom: 25px;
            }

            .table thead {
                color: #fff;
                font-weight: 900;
                text-transform: uppercase;
                background-color: blue;
            }        

        </style>
        
    </head>
    <body>
        <div class="container">
            <div class="row titleheader">
                <div class="title"><a href="{{ url('/') }}"><img src={{ asset("assets/images/asmph.jpg") }} width="300" /></a></div>
                <div class="subtitle">Adult Clinical Encounter Results</div>
            </div>

            <table id="resultsTable" class="table table-striped table-hover dt-responsive nowrap">
                <thead>
                <tr>
                    <th></th>
                    <th>ID #</th>
                    <th>Patient Name</th>
                    <th>Sex</th>
                    <th>Age</th>
                    <th>Date and Time of Consultation</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                    <?php $count = 1; ?>
                    @foreach($records as $key => $record)
                        <?php $exploded = json_decode($record); ?>
                        <tr>
                            <td scope="row">{{$count}} </td>
                            <td style="text-align:left"><a href="{{ url('update', $key) }}"> {{ $key }} </a></td>
                            <td style="text-align:left"> {{ $exploded->last_name}}, {{$exploded->first_name}} {{$exploded->middle_name}} </td>
                            <td style="text-align:left"> {{ $exploded->sex }} </td>
                            <td style="text-align:left"> {{ $exploded->age }} </td>
                            <td style="text-align:left"> {{ $exploded->interview_date }} </td>
                            <td><a href="{{ url('delete', $key) }}">Delete</a></td>
                        </tr>
                        <?php $count++; ?>
                    @endforeach                    
                </tbody>
            </table>
        </div>

        <!-- jQuery -->
        <script src={{ asset('assets\js\jquery-1.10.2.js') }}></script>
        <!-- Bootstrap Core JavaScript -->
        <script src={{ asset('assets\js\bootstrap.min.js') }}></script>
        <!-- jQuery DataTable -->
        <script src={{ asset('assets\js\jquery.dataTables.min.js') }}></script>
        <!-- jQuery DataTable -->
        <script src={{ asset('assets\js\dataTables.bootstrap.min.js') }}></script>
        <!-- jQuery DataTable -->
        <script src={{ asset('assets\js\dataTables.responsive.min.js') }}></script>
        <!-- jQuery DataTable -->
        <script src={{ asset('assets\js\responsive.bootstrap.min.js') }}></script>

         <script>
            $('#resultsTable').DataTable();
         </script>

    </body>
</html>
