<!DOCTYPE html>
<html>
    <head>
        <title>ASMPH</title>

        <link href="//fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 126px;
            }
            .subtitle {
                font-size: 26px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title"><a href="{{ url('/') }}"><img src={{ asset("assets/images/asmph.jpg") }} width="400" /></a></div>
                <div class="subtitle">Choose the entry form you need:</div>
                <div class="form-group">
                    <select class="form-control" onchange="goto(this.value);">
                        <option>Choose....</option>
                        <option value="01">Adult Clinical Encounter</option>
                        <option value="02">Geriatric Clinical Encounter</option>
                        <option value="03">Pedia Clinical Encounter</option>
                        <option value="04">Neonatal Patient</option>
                    </select>
                </div>
                <div class="subtitle"> or </div>
                <div class="subtitle"><a href="{{ url('admin') }}"> Go to Results </a></div>
            </div>
        </div>

        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <script>
            function goto(id)
            {
                if(id)
                {
                    location.href="loadform/"+id;
                }
            }
        </script>
    </body>
</html>
